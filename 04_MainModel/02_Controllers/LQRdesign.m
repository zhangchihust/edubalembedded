%% Mini Segway parameters

m = 0.933;          % body part mass [kg] 
R = 0.04;           % radius of wheel [m]
L = 0.085749;       % position of COM [m]
I = 0.00686014;     % inertia of body part [kg*m^2]       
Bm = 0;             % bearing damping ratio [N*m/(rad/s)]
g = 9.81;           % gravity constant [m/s^2]
Tau = 0.0994;       % speed loop time constant (P=0.194775 I=3.387779)
K = 1.0071;         % speed loop gain
Ts = 0.01;          % controller sampling time

  
%% State-space representation of the system 
% State variable: [Phi, Phidot, Theta, Thetadot]'  
% System matrix
A = [ 0          1                                0                0           ;...
      0        -1/Tau                             0                0           ;...
      0          0                                0                1           ;...
      0     (Bm+m*R*L/Tau)/(I+m*L^2)         m*g*L/(I+m*L^2)   -Bm/(I+m*L^2)  ];
% Input matrix
B = [0;K/Tau;0;-K*m*R*L/(Tau*(I+m*L^2))];
% Output matrix
C = [1 0 0 0];
% Feedforward matrix
D = 0;
%% System analysis

dim = size(A);        % dimension of the state transition matrix
n =  dim(1);          % system order
sys = ss(A,B,C,D);    % state space model of the system 
poles = eig(sys);     % poles of the system
G = tf(sys);          % transfer function of sys 
rlocus(G);            % place of poles and zeros

co = rank(ctrb(sys)); % check for rank of controllability matrix
if co == n
    disp('System is controllable');
else
    disp('System is not controllable');
end
%% LQR controller design 

Q = [1;1e-2;1;1e-2].*eye(4);        % weighting matrix Q 
R = 200;                            % weighting matrix R 
K = lqr(A,B,Q,R);                   % LQR gain matrix 
Kd = lqrd(A,B,Q,R,Ts);              % discrete LQR gain
sys1_lqr=ss(A-B*K,B,C,D);           % close-loop system
initial(sys1_lqr, [0; 0; 0.17; 0])  % free response 

%% Discrete time LQI controller design
sysd = c2d(sys,0.01,'zoh');         % discrete state space model
Qlqi = [10;0.1;0.1;0.1;1].*eye(5);  % state weighting matrix                   
Rlqi = 1000;                        % control input weighting
Klqid = lqi(sysd,Qlqi,Rlqi);        % LQI controller gain 
Kd2 = Klqid(1:end-1);               % extract the gains for the LQR regulator 
Kid = Klqid(end);                   % extract integral gain
Nbar = Kd2(1);                      % Feedforward gain (see FF controller design)


%% Feedforward gain design
% Md = [sysd.A sysd.B; sysd.C 0];                   % Steady state matrix from 
%                                                   % [u r]' to [x1 x2 x3 x4]'  
% Nxud = Md\[zeros(length(sysd.A),1);1];            % compute steady state gain 
%                                                   % matrix
% Nbard = Nxud(end) + Klqid(1:end-1)*Nxud(1:end-1); % calculate feedforward gain
