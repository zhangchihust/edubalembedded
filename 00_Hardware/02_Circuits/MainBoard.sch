<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.4.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="130bmp" color="2" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="2" fill="9" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DOCSMAL" urn="urn:adsk.eagle:symbol:13873/1" library_version="1">
<wire x1="88.9" y1="0" x2="88.9" y2="5.08" width="0.254" layer="94"/>
<wire x1="88.9" y1="5.08" x2="149.86" y2="5.08" width="0.254" layer="94"/>
<wire x1="149.86" y1="5.08" x2="149.86" y2="0" width="0.254" layer="94"/>
<wire x1="149.86" y1="5.08" x2="180.34" y2="5.08" width="0.254" layer="94"/>
<wire x1="88.9" y1="5.08" x2="88.9" y2="10.16" width="0.254" layer="94"/>
<wire x1="88.9" y1="10.16" x2="180.34" y2="10.16" width="0.254" layer="94"/>
<text x="90.17" y="6.35" size="2.54" layer="94">Date:</text>
<text x="101.6" y="6.35" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="151.13" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="165.1" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="90.17" y="1.27" size="2.54" layer="94">TITLE:</text>
<text x="106.68" y="1.27" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<frame x1="0" y1="0" x2="180.34" y2="264.16" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4-SMALL-DOCFIELD" urn="urn:adsk.eagle:component:13936/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, with small doc field</description>
<gates>
<gate name="/1" symbol="DOCSMAL" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MainBoard">
<packages>
<package name="ESP-12">
<wire x1="-8" y1="13" x2="8" y2="13" width="0.127" layer="21"/>
<wire x1="8" y1="13" x2="8" y2="-11" width="0.127" layer="21"/>
<wire x1="8" y1="-11" x2="-8" y2="-11" width="0.127" layer="21"/>
<wire x1="-8" y1="-11" x2="-8" y2="13" width="0.127" layer="21"/>
<wire x1="-6" y1="5.2" x2="6" y2="5.2" width="0.127" layer="21"/>
<wire x1="6" y1="5.2" x2="6" y2="-9.8" width="0.127" layer="21"/>
<wire x1="6" y1="-9.8" x2="-6" y2="-9.8" width="0.127" layer="21"/>
<wire x1="-6" y1="-9.8" x2="-6" y2="5.2" width="0.127" layer="21"/>
<text x="-0.1" y="8.3" size="1.27" layer="21" font="vector" ratio="12" align="center">Antenna</text>
<smd name="1" x="-7.5" y="4.5" dx="2" dy="1.2" layer="1"/>
<smd name="2" x="-7.5" y="2.5" dx="2" dy="1.2" layer="1"/>
<smd name="3" x="-7.5" y="0.5" dx="2" dy="1.2" layer="1"/>
<smd name="4" x="-7.5" y="-1.5" dx="2" dy="1.2" layer="1"/>
<smd name="5" x="-7.5" y="-3.5" dx="2" dy="1.2" layer="1"/>
<smd name="6" x="-7.5" y="-5.5" dx="2" dy="1.2" layer="1"/>
<smd name="7" x="-7.5" y="-7.5" dx="2" dy="1.2" layer="1"/>
<smd name="8" x="-7.5" y="-9.5" dx="2" dy="1.2" layer="1"/>
<smd name="9" x="7.5" y="-9.5" dx="2" dy="1.2" layer="1"/>
<smd name="10" x="7.5" y="-7.5" dx="2" dy="1.2" layer="1"/>
<smd name="11" x="7.5" y="-5.5" dx="2" dy="1.2" layer="1"/>
<smd name="12" x="7.5" y="-3.5" dx="2" dy="1.2" layer="1"/>
<smd name="13" x="7.5" y="-1.5" dx="2" dy="1.2" layer="1"/>
<smd name="14" x="7.5" y="0.5" dx="2" dy="1.2" layer="1"/>
<smd name="15" x="7.5" y="2.5" dx="2" dy="1.2" layer="1"/>
<smd name="16" x="7.5" y="4.5" dx="2" dy="1.2" layer="1"/>
<text x="-5" y="-12" size="1.4224" layer="25" font="vector" ratio="12" align="center">&gt;NAME</text>
<text x="4.5" y="-12" size="1.4224" layer="27" font="vector" ratio="12" align="center">&gt;VALUE</text>
<rectangle x1="-8" y1="6" x2="8" y2="13" layer="41"/>
<rectangle x1="-8" y1="6" x2="8" y2="13" layer="42"/>
</package>
<package name="LED-1608">
<wire x1="-1.3335" y1="0.635" x2="1.3335" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.3335" y1="0.635" x2="1.3335" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.3335" y1="-0.635" x2="-1.3335" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.3335" y1="-0.635" x2="-1.3335" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.381" x2="-0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-0.127" y1="-0.381" x2="0" y2="-0.1905" width="0.127" layer="21"/>
<wire x1="0" y1="-0.1905" x2="0.127" y2="0" width="0.127" layer="21"/>
<wire x1="0.127" y1="0" x2="0" y2="0.1905" width="0.127" layer="21"/>
<wire x1="0" y1="0.1905" x2="-0.127" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.0635" y1="0.254" x2="-0.0635" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0" y1="0.1905" x2="0" y2="-0.1905" width="0.127" layer="21"/>
<smd name="+" x="-0.762" y="0" dx="0.762" dy="0.889" layer="1" roundness="25"/>
<smd name="-" x="0.762" y="0" dx="0.762" dy="0.889" layer="1" roundness="25"/>
<text x="-1.905" y="0.889" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.143" y="-1.5875" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.5715" x2="1.27" y2="0.5715" layer="39"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.1" y="0.65"/>
<vertex x="0.1" y="0.65"/>
<vertex x="0.1" y="-0.65"/>
<vertex x="-0.1" y="-0.65"/>
</polygon>
</package>
<package name="SOT-23">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="-1.45" y1="0.65" x2="1.45" y2="0.65" width="0.1524" layer="21"/>
<wire x1="1.45" y1="0.65" x2="1.45" y2="-0.65" width="0.1524" layer="21"/>
<wire x1="1.45" y1="-0.65" x2="-1.45" y2="-0.65" width="0.1524" layer="21"/>
<wire x1="-1.45" y1="-0.65" x2="-1.45" y2="0.65" width="0.1524" layer="21"/>
<smd name="3" x="0.95" y="1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="-0.95" y="1" dx="0.8" dy="0.9" layer="1"/>
<smd name="1" x="0" y="-1" dx="0.8" dy="0.9" layer="1"/>
<text x="-1.397" y="1.778" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.397" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-0.22" y1="-1.2" x2="0.22" y2="-0.65" layer="51"/>
<rectangle x1="0.73" y1="0.65" x2="1.17" y2="1.2" layer="51" rot="R180"/>
<rectangle x1="-1.17" y1="0.65" x2="-0.73" y2="1.2" layer="51" rot="R180"/>
</package>
<package name="H6-2.54">
<wire x1="-1.27" y1="7.62" x2="1.27" y2="7.62" width="0.127" layer="21"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="-7.62" width="0.127" layer="21"/>
<wire x1="1.27" y1="-7.62" x2="-1.27" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-7.62" x2="-1.27" y2="7.62" width="0.127" layer="21"/>
<wire x1="-1.27" y1="7.62" x2="1.27" y2="7.62" width="0" layer="39"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="-7.62" width="0" layer="39"/>
<wire x1="1.27" y1="-7.62" x2="-1.27" y2="-7.62" width="0" layer="39"/>
<wire x1="-1.27" y1="-7.62" x2="-1.27" y2="7.62" width="0" layer="39"/>
<pad name="1" x="0" y="6.35" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="3.81" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="5" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<pad name="6" x="0" y="-6.35" drill="0.889" diameter="1.651"/>
<text x="2.54" y="-1.905" size="0.889" layer="27" ratio="11" rot="R90">&gt;value</text>
<text x="-1.905" y="-1.905" size="0.889" layer="25" ratio="11" rot="R90">&gt;name</text>
<text x="0.635" y="-2.54" size="0.635" layer="33" ratio="10" rot="R90">&gt;name</text>
<rectangle x1="-1.27" y1="-7.62" x2="1.27" y2="7.62" layer="39"/>
</package>
<package name="H4-2.54">
<wire x1="-1.27" y1="5.08" x2="1.27" y2="5.08" width="0.127" layer="21"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="1.27" y1="-5.08" x2="-1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="5.08" width="0.127" layer="21"/>
<wire x1="-1.27" y1="5.08" x2="1.27" y2="5.08" width="0" layer="39"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="-5.08" width="0" layer="39"/>
<wire x1="1.27" y1="-5.08" x2="-1.27" y2="-5.08" width="0" layer="39"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="5.08" width="0" layer="39"/>
<pad name="1" x="0" y="3.81" drill="0.889" diameter="1.651" shape="square"/>
<pad name="2" x="0" y="1.27" drill="0.889" diameter="1.651"/>
<pad name="3" x="0" y="-1.27" drill="0.889" diameter="1.651"/>
<pad name="4" x="0" y="-3.81" drill="0.889" diameter="1.651"/>
<text x="-1.905" y="-1.905" size="0.889" layer="25" ratio="11" rot="R90">&gt;name</text>
<text x="2.54" y="-1.905" size="0.889" layer="27" ratio="11" rot="R90">&gt;value</text>
<rectangle x1="-1.27" y1="-5.08" x2="1.27" y2="5.08" layer="39"/>
</package>
<package name="BZ2-7.6-D12.0XH6.5MM">
<circle x="0" y="0" radius="6.1" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.3" width="0.127" layer="21"/>
<pad name="+" x="-2.5" y="0" drill="0.9" diameter="1.5"/>
<pad name="-" x="2.5" y="0" drill="0.9" diameter="1.5"/>
<circle x="0" y="0" radius="6" width="0.127" layer="39"/>
<text x="-1.905" y="6.35" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
<wire x1="-4.445" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="2.54" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="1.27" width="0.254" layer="21"/>
</package>
<package name="SW4-6.0X6.0X5.0MM">
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<pad name="1" x="-3.25" y="2.25" drill="1.143" diameter="1.905" shape="square"/>
<pad name="2" x="-3.25" y="-2.25" drill="1.143" diameter="1.905"/>
<pad name="3" x="3.25" y="-2.25" drill="1.143" diameter="1.905"/>
<pad name="4" x="3.25" y="2.25" drill="1.143" diameter="1.905"/>
<rectangle x1="-3.1" y1="-3.1" x2="3.1" y2="3.1" layer="39"/>
<text x="-1.905" y="3.302" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.905" y="-0.635" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
</package>
<package name="LSM9DS0">
<pad name="1" x="-10.16" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="-7.62" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="-5.08" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="4" x="-2.54" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="5" x="0" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="6" x="2.54" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="7" x="5.08" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="8" x="7.62" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-14.0462" y="18.3388" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-13.97" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-7.874" y1="-0.254" x2="-7.366" y2="0.254" layer="51"/>
<rectangle x1="-10.414" y1="-0.254" x2="-9.906" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<circle x="-13.97" y="0" radius="1.27" width="0.127" layer="21"/>
<circle x="13.97" y="0" radius="1.27" width="0.127" layer="21"/>
<circle x="13.97" y="15.24" radius="1.27" width="0.127" layer="21"/>
<circle x="-13.97" y="15.24" radius="1.27" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="-13.97" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-16.51" y1="0" x2="-16.51" y2="15.24" width="0.127" layer="21"/>
<wire x1="-13.97" y1="17.78" x2="13.97" y2="17.78" width="0.127" layer="21"/>
<wire x1="16.51" y1="15.24" x2="16.51" y2="0" width="0.127" layer="21"/>
<wire x1="-16.51" y1="0" x2="-13.97" y2="-2.54" width="0.127" layer="21" curve="90"/>
<wire x1="-16.51" y1="15.24" x2="-13.97" y2="17.78" width="0.127" layer="21" curve="-90"/>
<wire x1="13.97" y1="17.78" x2="16.51" y2="15.24" width="0.127" layer="21" curve="-90"/>
<wire x1="16.51" y1="0" x2="13.97" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<pad name="10" x="-5.08" y="15.24" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="11" x="-2.54" y="15.24" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="12" x="0" y="15.24" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="13" x="2.54" y="15.24" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="14" x="5.08" y="15.24" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.5" y="0.8"/>
<vertex x="0.5" y="0.8"/>
<vertex x="0.5" y="-0.8"/>
<vertex x="-0.5" y="-0.8"/>
</polygon>
</package>
<package name="R6432">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.71" x2="2.387" y2="1.71" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.71" x2="2.387" y2="-1.71" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.325" y="0" dx="1" dy="3.65" layer="1" thermals="no"/>
<smd name="2" x="3.325" y="0" dx="1" dy="3.65" layer="1" thermals="no"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.325" y1="-1.775" x2="-2.275" y2="1.775" layer="51"/>
<rectangle x1="2.275" y1="-1.775" x2="3.325" y2="1.775" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<wire x1="-2.7" y1="1.7" x2="2.7" y2="1.7" width="0.127" layer="21"/>
<wire x1="2.7" y1="1.7" x2="2.7" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2.7" y1="-1.7" x2="-2.7" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2.7" y1="-1.7" x2="-2.7" y2="1.7" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-2.7" y="1.7"/>
<vertex x="-2.3" y="1.7"/>
<vertex x="-2.3" y="-1.7"/>
<vertex x="-2.7" y="-1.7"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="2.3" y="1.7"/>
<vertex x="2.7" y="1.7"/>
<vertex x="2.7" y="-1.7"/>
<vertex x="2.3" y="-1.7"/>
</polygon>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.073" y1="0.583" x2="1.073" y2="0.583" width="0.0508" layer="39"/>
<wire x1="1.073" y1="0.583" x2="1.073" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="1.073" y1="-0.583" x2="-1.073" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="-1.073" y1="-0.583" x2="-1.073" y2="0.583" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1" thermals="no"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1" thermals="no"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.2" y="0.25"/>
<vertex x="0.2" y="0.25"/>
<vertex x="0.2" y="-0.25"/>
<vertex x="-0.2" y="-0.25"/>
</polygon>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.683" x2="1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.683" x2="1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.683" x2="-1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.683" x2="-1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.2" y="0.42"/>
<vertex x="0.2" y="0.42"/>
<vertex x="0.2" y="-0.4"/>
<vertex x="-0.2" y="-0.4"/>
</polygon>
</package>
<package name="C0603">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1" stop="no" thermals="no" cream="no"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1" stop="no" thermals="no" cream="no"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
<rectangle x1="-0.425" y1="-0.225" x2="-0.075" y2="0.225" layer="29"/>
<rectangle x1="0.075" y1="-0.225" x2="0.425" y2="0.225" layer="29"/>
<rectangle x1="-0.425" y1="-0.225" x2="-0.075" y2="0.225" layer="31"/>
<rectangle x1="0.075" y1="-0.225" x2="0.425" y2="0.225" layer="31"/>
<wire x1="-0.5" y1="-0.3" x2="-0.5" y2="0.3" width="0.05" layer="39"/>
<wire x1="-0.5" y1="0.3" x2="0.5" y2="0.3" width="0.05" layer="39"/>
<wire x1="0.5" y1="0.3" x2="0.5" y2="-0.3" width="0.05" layer="39"/>
<wire x1="0.5" y1="-0.3" x2="-0.5" y2="-0.3" width="0.05" layer="39"/>
<wire x1="0" y1="0.07" x2="0" y2="-0.07" width="0.17" layer="21"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1" thermals="no"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1" thermals="no"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.5" y="1.25"/>
<vertex x="0.5" y="1.25"/>
<vertex x="0.5" y="-1.25"/>
<vertex x="-0.5" y="-1.25"/>
</polygon>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1" thermals="no"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1" thermals="no"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
<polygon width="0.127" layer="21">
<vertex x="-0.1" y="0.65"/>
<vertex x="0.1" y="0.65"/>
<vertex x="0.1" y="-0.65"/>
<vertex x="-0.1" y="-0.65"/>
</polygon>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.75" y1="-0.95" x2="-0.75" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.95" x2="0.75" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.35" dx="1.5" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.35" dx="1.5" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.8" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.8" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.8" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.8" y2="1.35" layer="51"/>
<rectangle x1="-0.8" y1="0.95" x2="0.8" y2="1.25" layer="51"/>
<rectangle x1="-0.8" y1="-1.65" x2="0.8" y2="-0.95" layer="51"/>
<polygon width="0.127" layer="21">
<vertex x="-0.6" y="-0.5"/>
<vertex x="0" y="0.3"/>
<vertex x="0.6" y="-0.5"/>
</polygon>
<wire x1="-0.6" y1="0.5" x2="0.6" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.75" y1="0.55" x2="-0.75" y2="-0.55" width="0.08" layer="21"/>
<wire x1="0.75" y1="-0.55" x2="0.75" y2="0.55" width="0.08" layer="21"/>
<wire x1="-0.9" y1="-2.1" x2="-0.9" y2="2.1" width="0.127" layer="39"/>
<wire x1="-0.9" y1="2.1" x2="0.9" y2="2.1" width="0.127" layer="39"/>
<wire x1="0.9" y1="2.1" x2="0.9" y2="-2.1" width="0.127" layer="39"/>
<wire x1="0.9" y1="-2.1" x2="-0.9" y2="-2.1" width="0.127" layer="39"/>
</package>
<package name="223AM">
<smd name="3" x="2.8" y="-0.85" dx="1.2" dy="1" layer="1"/>
<smd name="4" x="2.8" y="0.85" dx="1.2" dy="1" layer="1"/>
<smd name="2" x="-2.8" y="0.85" dx="1.2" dy="1" layer="1"/>
<smd name="1" x="-2.8" y="-0.85" dx="1.2" dy="1" layer="1"/>
<text x="-3.4" y="1.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.55" y="-3.25" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.35" y1="1.75" x2="2.35" y2="1.75" width="0.1016" layer="51"/>
<wire x1="2.35" y1="1.75" x2="2.35" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="2.35" y1="-1.75" x2="-2.35" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="-2.35" y1="-1.75" x2="-2.35" y2="1.75" width="0.1016" layer="51"/>
<wire x1="-2.35" y1="1.5" x2="-2.35" y2="1.75" width="0.127" layer="21"/>
<wire x1="-2.35" y1="1.75" x2="2.35" y2="1.75" width="0.127" layer="21"/>
<wire x1="2.35" y1="1.75" x2="2.35" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.35" y1="0.2" x2="2.35" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-2.35" y1="0.2" x2="-2.35" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-2.35" y1="-1.5" x2="-2.35" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-2.35" y1="-1.75" x2="2.35" y2="-1.75" width="0.127" layer="21"/>
<wire x1="2.35" y1="-1.75" x2="2.35" y2="-1.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="0.9013875" width="0.127" layer="21"/>
<circle x="0" y="0" radius="0.71063125" width="0.127" layer="21"/>
<circle x="0" y="0" radius="0.71063125" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="0.890221875" width="0.1016" layer="51"/>
<rectangle x1="2.35" y1="0.55" x2="3.2" y2="1.15" layer="51"/>
<rectangle x1="2.35" y1="-1.15" x2="3.2" y2="-0.55" layer="51"/>
<rectangle x1="-3.2" y1="-1.15" x2="-2.35" y2="-0.55" layer="51" rot="R180"/>
<rectangle x1="-3.2" y1="0.55" x2="-2.35" y2="1.15" layer="51" rot="R180"/>
<wire x1="3.6" y1="-1.9" x2="-3.6" y2="-1.9" width="0.1016" layer="39"/>
<wire x1="-3.6" y1="-1.9" x2="-3.6" y2="1.9" width="0.1016" layer="39"/>
<wire x1="-3.6" y1="1.9" x2="3.6" y2="1.9" width="0.1016" layer="39"/>
<wire x1="3.6" y1="1.9" x2="3.6" y2="-1.9" width="0.1016" layer="39"/>
</package>
<package name="8SOIC-N">
<smd name="1" x="-1.905" y="-2.7" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-0.635" y="-2.7" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="0.635" y="-2.7" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="1.905" y="-2.7" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="1.905" y="2.7" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="0.635" y="2.7" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="-0.635" y="2.7" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="-1.905" y="2.7" dx="1.55" dy="0.6" layer="1" rot="R90"/>
<wire x1="2.2225" y1="-1.5875" x2="-2.2225" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="-1.5875" x2="-2.2225" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="0.635" x2="-2.2225" y2="1.5875" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="1.5875" x2="2.2225" y2="1.5875" width="0.127" layer="21"/>
<wire x1="2.2225" y1="1.5875" x2="2.2225" y2="-1.5875" width="0.127" layer="21"/>
<wire x1="-2.2225" y1="0.635" x2="-2.2225" y2="-0.635" width="0.127" layer="21" curve="-180"/>
<circle x="-1.8" y="-1.2" radius="0.14141875" width="0.127" layer="21"/>
<wire x1="2.45" y1="-1.95" x2="-2.45" y2="-1.95" width="0.127" layer="51"/>
<wire x1="-2.45" y1="-1.95" x2="-2.45" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-2.45" y1="0.65" x2="-2.45" y2="1.95" width="0.127" layer="51"/>
<wire x1="-2.45" y1="1.95" x2="2.45" y2="1.95" width="0.127" layer="51"/>
<wire x1="2.45" y1="1.95" x2="2.45" y2="-1.95" width="0.127" layer="51"/>
<circle x="-1.8" y="-1.2" radius="0.14141875" width="0.127" layer="51"/>
<wire x1="-2.45" y1="0.65" x2="-2.45" y2="-0.65" width="0.127" layer="51" curve="-180"/>
<rectangle x1="-2.11" y1="-3.04" x2="-1.7" y2="-2" layer="51"/>
<rectangle x1="-0.84" y1="-3.04" x2="-0.43" y2="-2" layer="51"/>
<rectangle x1="0.43" y1="-3.04" x2="0.84" y2="-2" layer="51"/>
<rectangle x1="1.7" y1="-3.04" x2="2.11" y2="-2" layer="51"/>
<rectangle x1="1.7" y1="2" x2="2.11" y2="3.04" layer="51" rot="R180"/>
<rectangle x1="0.43" y1="2" x2="0.84" y2="3.04" layer="51" rot="R180"/>
<rectangle x1="-0.84" y1="2" x2="-0.43" y2="3.04" layer="51" rot="R180"/>
<rectangle x1="-2.11" y1="2" x2="-1.7" y2="3.04" layer="51" rot="R180"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.6" y1="3.7" x2="2.6" y2="3.7" width="0.127" layer="39"/>
<wire x1="2.6" y1="3.7" x2="2.6" y2="-3.7" width="0.127" layer="39"/>
<wire x1="2.6" y1="-3.7" x2="-2.6" y2="-3.7" width="0.127" layer="39"/>
<wire x1="-2.6" y1="-3.7" x2="-2.6" y2="3.7" width="0.127" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="ESP8266-12">
<wire x1="-12.7" y1="15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="-12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="-5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="-2.54" y1="12.7" x2="0" y2="12.7" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="12.7" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<pin name="RESET" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="ADC" x="-17.78" y="2.54" length="middle" direction="pas"/>
<pin name="CH_PD" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="GPIO16" x="-17.78" y="-2.54" length="middle"/>
<pin name="GPIO14" x="-17.78" y="-5.08" length="middle"/>
<pin name="GPIO12" x="-17.78" y="-7.62" length="middle"/>
<pin name="GPIO13" x="-17.78" y="-10.16" length="middle"/>
<pin name="VCC" x="-17.78" y="-12.7" length="middle" direction="pwr"/>
<pin name="GND" x="17.78" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="GPIO15" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="GPIO2" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="GPIO0" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="GPIO5" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="GPIO4" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="RXD" x="17.78" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="TXD" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<text x="0" y="0" size="1.778" layer="94" font="vector" ratio="12" rot="R90" align="center">ESP12</text>
</symbol>
<symbol name="LED-1">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.143" y2="2.413" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="0.889" y2="4.445" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.127" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.524" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="1.397" y2="3.048" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="HEADER-6P">
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-8.89" y="8.89" size="1.27" layer="95" ratio="10">&gt;name</text>
<text x="-2.54" y="8.89" size="1.27" layer="96" ratio="10">&gt;value</text>
<pin name="1" x="-7.62" y="6.35" visible="pad" length="middle" function="dotclk"/>
<pin name="2" x="-7.62" y="3.81" visible="pad" length="middle"/>
<pin name="3" x="-7.62" y="1.27" visible="pad" length="middle"/>
<pin name="4" x="-7.62" y="-1.27" visible="pad" length="middle"/>
<pin name="5" x="-7.62" y="-3.81" visible="pad" length="middle"/>
<pin name="6" x="-7.62" y="-6.35" visible="pad" length="middle"/>
</symbol>
<symbol name="HEADER-4P">
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="6.35" size="1.27" layer="95" ratio="10">&gt;name</text>
<text x="-1.27" y="6.35" size="1.27" layer="96" ratio="10">&gt;value</text>
<pin name="P$1" x="-7.62" y="3.81" visible="pad" length="middle" function="dotclk"/>
<pin name="P$2" x="-7.62" y="1.27" visible="pad" length="middle"/>
<pin name="P$3" x="-7.62" y="-1.27" visible="pad" length="middle"/>
<pin name="P$4" x="-7.62" y="-3.81" visible="pad" length="middle"/>
</symbol>
<symbol name="BUZZER">
<pin name="+" x="-7.62" y="0" length="short"/>
<pin name="-" x="7.62" y="0" length="short" rot="R180"/>
<wire x1="-5.08" y1="0" x2="-3.683" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="3.683" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="3.5921" width="0.1524" layer="94"/>
<text x="-7.62" y="3.81" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="1.27" y="3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
</symbol>
<symbol name="BOTTON-4P">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.08" y="5.08" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-6.35" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="A0" x="-8.89" y="2.54" length="short"/>
<pin name="A1" x="8.89" y="2.54" length="short" rot="R180"/>
<pin name="B0" x="-8.89" y="-2.54" length="short"/>
<pin name="B1" x="8.89" y="-2.54" length="short" rot="R180"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<circle x="-1.27" y="-0.762" radius="0.1524" width="0" layer="94"/>
<circle x="0" y="-1.27" radius="0.1524" width="0" layer="94"/>
</symbol>
<symbol name="LSM9DS0">
<pin name="VIN" x="-10.16" y="-13.97" length="middle" rot="R90"/>
<pin name="3V3" x="-7.62" y="-13.97" length="middle" rot="R90"/>
<pin name="GND" x="-5.08" y="-13.97" length="middle" rot="R90"/>
<pin name="SCL" x="-2.54" y="-13.97" length="middle" rot="R90"/>
<pin name="SDA" x="0" y="-13.97" length="middle" rot="R90"/>
<pin name="CSG" x="2.54" y="-13.97" length="middle" rot="R90"/>
<pin name="CSXM" x="5.08" y="-13.97" length="middle" rot="R90"/>
<pin name="SDOG" x="7.62" y="-13.97" length="middle" rot="R90"/>
<pin name="SDOXM" x="10.16" y="-13.97" length="middle" rot="R90"/>
<pin name="DEN" x="-5.08" y="15.24" length="middle" rot="R270"/>
<pin name="INT2" x="-2.54" y="15.24" length="middle" rot="R270"/>
<pin name="INT1" x="0" y="15.24" length="middle" rot="R270"/>
<pin name="DRDY" x="2.54" y="15.24" length="middle" rot="R270"/>
<pin name="INTG" x="5.08" y="15.24" length="middle" rot="R270"/>
<wire x1="-13.97" y1="10.16" x2="13.97" y2="10.16" width="0.254" layer="94"/>
<wire x1="16.51" y1="7.62" x2="16.51" y2="-6.35" width="0.254" layer="94"/>
<wire x1="13.97" y1="-8.89" x2="-13.97" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-6.35" x2="-16.51" y2="7.62" width="0.254" layer="94"/>
<circle x="-13.97" y="7.62" radius="1.27" width="0.254" layer="94"/>
<circle x="-13.97" y="-6.35" radius="1.27" width="0.254" layer="94"/>
<circle x="13.97" y="-6.35" radius="1.27" width="0.254" layer="94"/>
<circle x="13.97" y="7.62" radius="1.27" width="0.254" layer="94"/>
<wire x1="-13.97" y1="-8.89" x2="-16.51" y2="-6.35" width="0.254" layer="94" curve="-90"/>
<wire x1="-16.51" y1="7.62" x2="-13.97" y2="10.16" width="0.254" layer="94" curve="-90"/>
<wire x1="13.97" y1="10.16" x2="16.51" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="16.51" y1="-6.35" x2="13.97" y2="-8.89" width="0.254" layer="94" curve="-90"/>
<text x="-15.24" y="11.43" size="1.27" layer="95">&gt;NAME</text>
<text x="8.89" y="11.43" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="0" y="2.54" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="0" y="-5.08" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
</symbol>
<symbol name="24LC256">
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="A0" x="-12.7" y="2.54" length="middle"/>
<pin name="A1" x="-12.7" y="0" length="middle"/>
<pin name="A2" x="-12.7" y="-2.54" length="middle"/>
<pin name="VSS" x="-12.7" y="-5.08" length="middle"/>
<pin name="SDA" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="SCL" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="WP" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="VCC" x="12.7" y="2.54" length="middle" rot="R180"/>
<text x="-7.62" y="6.35" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP-12" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="ESP8266-12" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESP-12">
<connects>
<connect gate="G$1" pin="ADC" pad="2"/>
<connect gate="G$1" pin="CH_PD" pad="3"/>
<connect gate="G$1" pin="GND" pad="9"/>
<connect gate="G$1" pin="GPIO0" pad="12"/>
<connect gate="G$1" pin="GPIO12" pad="6"/>
<connect gate="G$1" pin="GPIO13" pad="7"/>
<connect gate="G$1" pin="GPIO14" pad="5"/>
<connect gate="G$1" pin="GPIO15" pad="10"/>
<connect gate="G$1" pin="GPIO16" pad="4"/>
<connect gate="G$1" pin="GPIO2" pad="11"/>
<connect gate="G$1" pin="GPIO4" pad="14"/>
<connect gate="G$1" pin="GPIO5" pad="13"/>
<connect gate="G$1" pin="RESET" pad="1"/>
<connect gate="G$1" pin="RXD" pad="15"/>
<connect gate="G$1" pin="TXD" pad="16"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED-SMD" prefix="D" uservalue="yes">
<description>304090042</description>
<gates>
<gate name="G$1" symbol="LED-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-1608">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="19-217-R6C-AL1M2VY-3T"/>
<attribute name="VALUE" value="RED-0603" constant="no"/>
</technology>
</technologies>
</device>
<device name="B" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R">
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="R6432">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NPNSOT23" prefix="T" uservalue="yes">
<description>&lt;b&gt;NPN TRANSISTOR&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="A" pin="B" pad="3"/>
<connect gate="A" pin="C" pad="1"/>
<connect gate="A" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="DSS20201L-7" constant="no"/>
<attribute name="OC_FARNELL" value="1710688" constant="no"/>
<attribute name="OC_NEWARK" value="25R4555" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIP-HEADER-6P" prefix="J" uservalue="yes">
<description>320020078</description>
<gates>
<gate name="G$1" symbol="HEADER-6P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="H6-2.54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="P125-1106A1BS116A1" constant="no"/>
<attribute name="VALUE" value="6p-2.54" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIP-HEADER-4P">
<gates>
<gate name="G$1" symbol="HEADER-4P" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="H4-2.54">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIP-BUZZER" prefix="BUZ" uservalue="yes">
<description>312010004</description>
<gates>
<gate name="G$1" symbol="BUZZER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BZ2-7.6-D12.0XH6.5MM">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="KS-12065D03YA" constant="no"/>
<attribute name="VALUE" value="KS-12065D03YA" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIP-BUTTON-4P" prefix="K" uservalue="yes">
<description>311020024</description>
<gates>
<gate name="G$1" symbol="BOTTON-4P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SW4-6.0X6.0X5.0MM">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="4"/>
<connect gate="G$1" pin="B0" pad="2"/>
<connect gate="G$1" pin="B1" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="TS-1109" constant="no"/>
<attribute name="VALUE" value="TS-1109"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="9D0F-LSM9DS0">
<gates>
<gate name="G$1" symbol="LSM9DS0" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="LSM9DS0">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="CSG" pad="6"/>
<connect gate="G$1" pin="CSXM" pad="7"/>
<connect gate="G$1" pin="DEN" pad="10"/>
<connect gate="G$1" pin="DRDY" pad="13"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="INT1" pad="12"/>
<connect gate="G$1" pin="INT2" pad="11"/>
<connect gate="G$1" pin="INTG" pad="14"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="SDOG" pad="8"/>
<connect gate="G$1" pin="SDOXM" pad="9"/>
<connect gate="G$1" pin="VIN" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C">
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="1.27"/>
</gates>
<devices>
<device name="A" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUTTON_SMD">
<gates>
<gate name="G$1" symbol="BOTTON-4P" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="223AM">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="3"/>
<connect gate="G$1" pin="B0" pad="2"/>
<connect gate="G$1" pin="B1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EEPROM-256K">
<gates>
<gate name="G$1" symbol="24LC256" x="0" y="0"/>
</gates>
<devices>
<device name="" package="8SOIC-N">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="WP" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="motor_driver">
<packages>
<package name="4X10-BOOSTERPACK-BOTTOM">
<wire x1="1.27" y1="-5.715" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-4.445" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-5.715" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="6.35" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="5.715" x2="-0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="0.635" y1="8.89" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="6.35" x2="-1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="8.255" x2="-0.635" y2="8.89" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-6.985" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-8.255" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-9.525" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-10.795" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-13.97" x2="-0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-13.97" x2="-1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-12.065" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-13.335" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="41" x="0" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="42" x="0" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="43" x="0" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="44" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="45" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="46" x="0" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="47" x="0" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="48" x="0" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="49" x="0" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="50" x="0" y="-12.7" drill="1.016" diameter="1.8796"/>
<text x="4.7498" y="11.4808" size="1.27" layer="25" font="vector" ratio="10">J5</text>
<rectangle x1="-0.254" y1="-5.334" x2="0.254" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-2.794" x2="0.254" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="4.826" x2="0.254" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="7.366" x2="0.254" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="9.906" x2="0.254" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-7.874" x2="0.254" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-10.414" x2="0.254" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-12.954" x2="0.254" y2="-12.446" layer="51" rot="R270"/>
<wire x1="-0.635" y1="8.89" x2="-1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="11.43" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="0.635" y2="8.89" width="0.2286" layer="21"/>
<text x="7.7978" y="11.4808" size="1.27" layer="25" font="vector" ratio="10">J7</text>
<text x="37.7698" y="11.4808" size="1.27" layer="25" font="vector" ratio="10" align="bottom-right">J8</text>
<text x="40.8178" y="11.4808" size="1.27" layer="25" font="vector" ratio="10" align="bottom-right">J6</text>
<wire x1="3.81" y1="-4.445" x2="3.81" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-5.715" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-4.445" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-5.715" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="6.35" x2="3.81" y2="5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="5.715" x2="3.81" y2="4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="4.445" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="5.715" x2="1.905" y2="6.35" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.175" y1="8.89" x2="3.81" y2="8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="8.255" x2="3.81" y2="6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.175" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="6.35" x2="1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="8.255" x2="1.905" y2="8.89" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.81" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-8.255" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.81" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-10.795" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.81" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-13.335" x2="3.175" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-13.97" x2="1.905" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-13.97" x2="1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="61" x="2.54" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="62" x="2.54" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="63" x="2.54" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="64" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="65" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="66" x="2.54" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="67" x="2.54" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="68" x="2.54" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="69" x="2.54" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="70" x="2.54" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="2.286" y1="-5.334" x2="2.794" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-2.794" x2="2.794" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="4.826" x2="2.794" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="7.366" x2="2.794" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="9.906" x2="2.794" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-7.874" x2="2.794" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-10.414" x2="2.794" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-12.954" x2="2.794" y2="-12.446" layer="51" rot="R270"/>
<wire x1="1.905" y1="8.89" x2="1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="11.43" x2="3.81" y2="11.43" width="0.2286" layer="21"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="8.89" width="0.2286" layer="21"/>
<wire x1="3.81" y1="8.89" x2="3.175" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-1.27" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-3.175" x2="41.91" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-1.905" x2="42.545" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-4.445" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-5.715" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.2032" layer="21"/>
<wire x1="43.815" y1="1.27" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="43.815" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-1.27" x2="41.91" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-0.635" x2="41.91" y2="0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="0.635" x2="42.545" y2="1.27" width="0.2032" layer="21"/>
<wire x1="43.815" y1="6.35" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="4.445" x2="41.91" y2="5.715" width="0.2032" layer="21"/>
<wire x1="41.91" y1="5.715" x2="42.545" y2="6.35" width="0.2032" layer="21"/>
<wire x1="44.45" y1="3.175" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="43.815" y1="8.89" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="43.815" y2="6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="6.35" x2="41.91" y2="6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="6.985" x2="41.91" y2="8.255" width="0.2032" layer="21"/>
<wire x1="41.91" y1="8.255" x2="42.545" y2="8.89" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-6.985" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-8.255" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-9.525" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-10.795" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="43.815" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-13.97" x2="42.545" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-13.97" x2="41.91" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-12.065" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-13.335" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<pad name="80" x="43.18" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="79" x="43.18" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="78" x="43.18" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="77" x="43.18" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="76" x="43.18" y="0" drill="1.016" diameter="1.8796"/>
<pad name="75" x="43.18" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="74" x="43.18" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="73" x="43.18" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="72" x="43.18" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="71" x="43.18" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="42.926" y1="-5.334" x2="43.434" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-2.794" x2="43.434" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-0.254" x2="43.434" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="2.286" x2="43.434" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="4.826" x2="43.434" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="7.366" x2="43.434" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="9.906" x2="43.434" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-7.874" x2="43.434" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-10.414" x2="43.434" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-12.954" x2="43.434" y2="-12.446" layer="51" rot="R270"/>
<wire x1="42.545" y1="8.89" x2="41.91" y2="8.89" width="0.2286" layer="21"/>
<wire x1="41.91" y1="8.89" x2="41.91" y2="11.43" width="0.2286" layer="21"/>
<wire x1="41.91" y1="11.43" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="43.815" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.99" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-5.715" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-1.27" x2="46.99" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-1.905" x2="46.99" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-3.175" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-1.905" x2="45.085" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="1.27" x2="44.45" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.355" y1="1.27" x2="46.99" y2="0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="0.635" x2="46.99" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-0.635" x2="46.355" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-1.27" x2="44.45" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="0.635" x2="45.085" y2="1.27" width="0.2032" layer="21"/>
<wire x1="46.355" y1="6.35" x2="46.99" y2="5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="5.715" x2="46.99" y2="4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="4.445" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="5.715" x2="45.085" y2="6.35" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="46.355" y1="8.89" x2="46.99" y2="8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="8.255" x2="46.99" y2="6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="6.985" x2="46.355" y2="6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="6.35" x2="44.45" y2="6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="8.255" x2="45.085" y2="8.89" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.99" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-8.255" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.99" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-10.795" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.99" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-13.335" x2="46.355" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-13.97" x2="45.085" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-13.97" x2="44.45" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<pad name="60" x="45.72" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="59" x="45.72" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="58" x="45.72" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="57" x="45.72" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="56" x="45.72" y="0" drill="1.016" diameter="1.8796"/>
<pad name="55" x="45.72" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="54" x="45.72" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="53" x="45.72" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="52" x="45.72" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="51" x="45.72" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="45.466" y1="-5.334" x2="45.974" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-2.794" x2="45.974" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-0.254" x2="45.974" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="2.286" x2="45.974" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="4.826" x2="45.974" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="7.366" x2="45.974" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="9.906" x2="45.974" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-7.874" x2="45.974" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-10.414" x2="45.974" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-12.954" x2="45.974" y2="-12.446" layer="51" rot="R270"/>
<wire x1="45.085" y1="8.89" x2="44.45" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="11.43" x2="46.99" y2="11.43" width="0.2286" layer="21"/>
<wire x1="46.99" y1="11.43" x2="46.99" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="8.89" x2="46.355" y2="8.89" width="0.2286" layer="21"/>
<text x="7.62" y="9.652" size="0.889" layer="21" font="vector">5V</text>
<text x="7.62" y="7.112" size="0.889" layer="21" font="vector">GND</text>
<text x="7.62" y="4.572" size="0.889" layer="21" font="vector">AB7</text>
<text x="7.62" y="-10.668" size="0.889" layer="21" font="vector">AA4</text>
<text x="-0.254" y="11.938" size="0.889" layer="21" font="vector">41</text>
<text x="3.556" y="11.938" size="0.889" layer="21" font="vector" align="bottom-right">61</text>
<text x="-0.762" y="-15.24" size="0.889" layer="21" font="vector">50</text>
<text x="3.556" y="-15.24" size="0.889" layer="21" font="vector" align="bottom-right">70</text>
<text x="7.62" y="2.032" size="0.889" layer="21" font="vector">AB4</text>
<text x="7.62" y="-0.508" size="0.889" layer="21" font="vector">AA5</text>
<text x="7.62" y="-3.048" size="0.889" layer="21" font="vector">AB5</text>
<text x="7.62" y="-5.588" size="0.889" layer="21" font="vector">AA3</text>
<text x="7.62" y="-8.128" size="0.889" layer="21" font="vector">AB3</text>
<text x="42.418" y="11.938" size="0.889" layer="21" font="vector">80</text>
<text x="46.482" y="11.938" size="0.889" layer="21" font="vector" align="bottom-right">60</text>
<text x="42.672" y="-15.24" size="0.889" layer="21" font="vector">71</text>
<text x="46.736" y="-15.24" size="0.889" layer="21" font="vector" align="bottom-right">51</text>
<text x="37.338" y="7.112" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="4.572" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="2.032" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-0.508" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-3.048" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-10.668" size="0.889" layer="21" font="vector" align="bottom-right">DAC</text>
<text x="37.338" y="-13.208" size="0.889" layer="21" font="vector" align="bottom-right">DAC</text>
<text x="41.148" y="9.652" size="0.889" layer="21" font="vector" align="bottom-right">GND</text>
<text x="41.148" y="7.112" size="0.889" layer="21" font="vector" align="bottom-right">P27</text>
<text x="41.148" y="4.572" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-10.668" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-0.508" size="0.889" layer="21" font="vector" align="bottom-right">RST</text>
<text x="41.148" y="-3.048" size="0.889" layer="21" font="vector" align="bottom-right">MOSI</text>
<text x="41.148" y="-5.588" size="0.889" layer="21" font="vector" align="bottom-right">MISO</text>
<text x="41.148" y="-8.128" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-13.208" size="0.889" layer="21" font="vector" align="bottom-right">P56</text>
<text x="37.338" y="9.652" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="4.572" y="9.652" size="0.889" layer="21" font="vector">3V3</text>
<text x="4.572" y="4.572" size="0.889" layer="21" font="vector">RX</text>
<text x="4.572" y="2.032" size="0.889" layer="21" font="vector">TX</text>
<text x="4.572" y="-0.508" size="0.889" layer="21" font="vector">P20</text>
<text x="4.572" y="-5.588" size="0.889" layer="21" font="vector">CLK</text>
<text x="4.572" y="-8.128" size="0.889" layer="21" font="vector">P21</text>
<text x="4.572" y="-10.668" size="0.889" layer="21" font="vector">P23</text>
<text x="4.572" y="-13.208" size="0.889" layer="21" font="vector">P54</text>
<wire x1="7.112" y1="10.668" x2="7.112" y2="-13.208" width="0.0508" layer="21"/>
<wire x1="37.846" y1="10.668" x2="37.846" y2="-13.208" width="0.0508" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="HEADER4X10-BOOSTERPACK-BOTTOM">
<wire x1="-2.54" y1="-12.7" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="15.24" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="15.24" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$1" x="5.08" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$2" x="5.08" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$3" x="5.08" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$4" x="5.08" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$5" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$6" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$7" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$8" x="5.08" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$9" x="5.08" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$10" x="5.08" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="53.34" y1="-12.7" x2="53.34" y2="15.24" width="0.254" layer="94"/>
<wire x1="53.34" y1="15.24" x2="58.42" y2="15.24" width="0.254" layer="94"/>
<wire x1="58.42" y1="15.24" x2="58.42" y2="-12.7" width="0.254" layer="94"/>
<wire x1="58.42" y1="-12.7" x2="53.34" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$20" x="60.96" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$19" x="60.96" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$18" x="60.96" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$17" x="60.96" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$16" x="60.96" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$15" x="60.96" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$14" x="60.96" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$13" x="60.96" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$12" x="60.96" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$11" x="60.96" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$21" x="20.32" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$22" x="20.32" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$23" x="20.32" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$24" x="20.32" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$25" x="20.32" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$26" x="20.32" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$27" x="20.32" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$28" x="20.32" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$29" x="20.32" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$30" x="20.32" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="38.1" y1="-12.7" x2="38.1" y2="15.24" width="0.254" layer="94"/>
<wire x1="38.1" y1="15.24" x2="43.18" y2="15.24" width="0.254" layer="94"/>
<wire x1="43.18" y1="15.24" x2="43.18" y2="-12.7" width="0.254" layer="94"/>
<wire x1="43.18" y1="-12.7" x2="38.1" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$40" x="45.72" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$39" x="45.72" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$38" x="45.72" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$37" x="45.72" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$36" x="45.72" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$35" x="45.72" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$34" x="45.72" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$33" x="45.72" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$32" x="45.72" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$31" x="45.72" y="-10.16" visible="pad" length="short" rot="R180"/>
<text x="-2.54" y="17.78" size="2.54" layer="95" font="vector">J5</text>
<text x="53.34" y="17.78" size="2.54" layer="95" font="vector">J6</text>
<text x="12.7" y="17.78" size="2.54" layer="95" font="vector">J7</text>
<text x="38.1" y="17.78" size="2.54" layer="95" font="vector">J8</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HEADER4X10-BOOSTERPACK-BOTTOM" prefix="A" uservalue="yes">
<gates>
<gate name="G$1" symbol="HEADER4X10-BOOSTERPACK-BOTTOM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4X10-BOOSTERPACK-BOTTOM">
<connects>
<connect gate="G$1" pin="P$1" pad="41"/>
<connect gate="G$1" pin="P$10" pad="50"/>
<connect gate="G$1" pin="P$11" pad="51"/>
<connect gate="G$1" pin="P$12" pad="52"/>
<connect gate="G$1" pin="P$13" pad="53"/>
<connect gate="G$1" pin="P$14" pad="54"/>
<connect gate="G$1" pin="P$15" pad="55"/>
<connect gate="G$1" pin="P$16" pad="56"/>
<connect gate="G$1" pin="P$17" pad="57"/>
<connect gate="G$1" pin="P$18" pad="58"/>
<connect gate="G$1" pin="P$19" pad="59"/>
<connect gate="G$1" pin="P$2" pad="42"/>
<connect gate="G$1" pin="P$20" pad="60"/>
<connect gate="G$1" pin="P$21" pad="61"/>
<connect gate="G$1" pin="P$22" pad="62"/>
<connect gate="G$1" pin="P$23" pad="63"/>
<connect gate="G$1" pin="P$24" pad="64"/>
<connect gate="G$1" pin="P$25" pad="65"/>
<connect gate="G$1" pin="P$26" pad="66"/>
<connect gate="G$1" pin="P$27" pad="67"/>
<connect gate="G$1" pin="P$28" pad="68"/>
<connect gate="G$1" pin="P$29" pad="69"/>
<connect gate="G$1" pin="P$3" pad="43"/>
<connect gate="G$1" pin="P$30" pad="70"/>
<connect gate="G$1" pin="P$31" pad="71"/>
<connect gate="G$1" pin="P$32" pad="72"/>
<connect gate="G$1" pin="P$33" pad="73"/>
<connect gate="G$1" pin="P$34" pad="74"/>
<connect gate="G$1" pin="P$35" pad="75"/>
<connect gate="G$1" pin="P$36" pad="76"/>
<connect gate="G$1" pin="P$37" pad="77"/>
<connect gate="G$1" pin="P$38" pad="78"/>
<connect gate="G$1" pin="P$39" pad="79"/>
<connect gate="G$1" pin="P$4" pad="44"/>
<connect gate="G$1" pin="P$40" pad="80"/>
<connect gate="G$1" pin="P$5" pad="45"/>
<connect gate="G$1" pin="P$6" pad="46"/>
<connect gate="G$1" pin="P$7" pad="47"/>
<connect gate="G$1" pin="P$8" pad="48"/>
<connect gate="G$1" pin="P$9" pad="49"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="EAGLE_BoosterPack_Library">
<packages>
<package name="4X10-BOOSTERPACK">
<wire x1="1.27" y1="-5.715" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-4.445" x2="0.635" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-5.715" x2="-1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="6.35" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="5.715" x2="-0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="0.635" y1="8.89" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="6.35" x2="-1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="8.255" x2="-0.635" y2="8.89" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-6.985" x2="0.635" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-6.35" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-8.255" x2="-1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-9.525" x2="0.635" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-8.89" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-10.795" x2="-1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-13.97" x2="-0.635" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-13.97" x2="-1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-12.065" x2="0.635" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-11.43" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-13.335" x2="-1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="1" x="0" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="0" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="3" x="0" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="4" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="0" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="0" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="8" x="0" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="9" x="0" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="10" x="0" y="-12.7" drill="1.016" diameter="1.8796"/>
<text x="4.7498" y="11.4808" size="1.27" layer="25" font="vector" ratio="10">J1</text>
<rectangle x1="-0.254" y1="-5.334" x2="0.254" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-2.794" x2="0.254" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="4.826" x2="0.254" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="7.366" x2="0.254" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="9.906" x2="0.254" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-7.874" x2="0.254" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-10.414" x2="0.254" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="-0.254" y1="-12.954" x2="0.254" y2="-12.446" layer="51" rot="R270"/>
<wire x1="-0.635" y1="8.89" x2="-1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="11.43" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="0.635" y2="8.89" width="0.2286" layer="21"/>
<text x="7.7978" y="11.4808" size="1.27" layer="25" font="vector" ratio="10">J3</text>
<text x="37.7698" y="11.4808" size="1.27" layer="25" font="vector" ratio="10" align="bottom-right">J4</text>
<text x="40.8178" y="11.4808" size="1.27" layer="25" font="vector" ratio="10" align="bottom-right">J2</text>
<wire x1="3.81" y1="-4.445" x2="3.81" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-5.715" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="1.27" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-4.445" x2="3.175" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-3.81" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-5.715" x2="1.27" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="6.35" x2="3.81" y2="5.715" width="0.2032" layer="21"/>
<wire x1="3.81" y1="5.715" x2="3.81" y2="4.445" width="0.2032" layer="21"/>
<wire x1="3.81" y1="4.445" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="5.715" x2="1.905" y2="6.35" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.175" y1="8.89" x2="3.81" y2="8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="8.255" x2="3.81" y2="6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.175" y2="6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="6.35" x2="1.27" y2="6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.255" width="0.2032" layer="21"/>
<wire x1="1.27" y1="8.255" x2="1.905" y2="8.89" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.81" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-8.255" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="3.175" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-8.255" x2="1.27" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.81" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-10.795" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-9.525" x2="3.175" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-8.89" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-10.795" x2="1.27" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.81" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-13.335" x2="3.175" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-13.97" x2="1.905" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-13.97" x2="1.27" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.065" x2="3.175" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-11.43" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-13.335" x2="1.27" y2="-12.065" width="0.2032" layer="21"/>
<pad name="21" x="2.54" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="22" x="2.54" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="23" x="2.54" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="24" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="25" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="26" x="2.54" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="27" x="2.54" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="28" x="2.54" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="29" x="2.54" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="30" x="2.54" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="2.286" y1="-5.334" x2="2.794" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-2.794" x2="2.794" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="4.826" x2="2.794" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="7.366" x2="2.794" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="9.906" x2="2.794" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-7.874" x2="2.794" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-10.414" x2="2.794" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="2.286" y1="-12.954" x2="2.794" y2="-12.446" layer="51" rot="R270"/>
<wire x1="1.905" y1="8.89" x2="1.27" y2="8.89" width="0.2286" layer="21"/>
<wire x1="1.27" y1="8.89" x2="1.27" y2="11.43" width="0.2286" layer="21"/>
<wire x1="1.27" y1="11.43" x2="3.81" y2="11.43" width="0.2286" layer="21"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="8.89" width="0.2286" layer="21"/>
<wire x1="3.81" y1="8.89" x2="3.175" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-1.27" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-3.175" x2="41.91" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-1.905" x2="42.545" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-4.445" x2="43.815" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-3.81" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-5.715" x2="41.91" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.2032" layer="21"/>
<wire x1="43.815" y1="1.27" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="43.815" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-1.27" x2="41.91" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-0.635" x2="41.91" y2="0.635" width="0.2032" layer="21"/>
<wire x1="41.91" y1="0.635" x2="42.545" y2="1.27" width="0.2032" layer="21"/>
<wire x1="43.815" y1="6.35" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="4.445" width="0.2032" layer="21"/>
<wire x1="41.91" y1="4.445" x2="41.91" y2="5.715" width="0.2032" layer="21"/>
<wire x1="41.91" y1="5.715" x2="42.545" y2="6.35" width="0.2032" layer="21"/>
<wire x1="44.45" y1="3.175" x2="43.815" y2="3.81" width="0.2032" layer="21"/>
<wire x1="42.545" y1="3.81" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.91" y2="3.175" width="0.2032" layer="21"/>
<wire x1="43.815" y1="8.89" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="43.815" y2="6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="6.35" x2="41.91" y2="6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="6.985" x2="41.91" y2="8.255" width="0.2032" layer="21"/>
<wire x1="41.91" y1="8.255" x2="42.545" y2="8.89" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-6.985" x2="43.815" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-8.255" x2="41.91" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-9.525" x2="43.815" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-8.89" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-10.795" x2="41.91" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="43.815" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="43.815" y1="-13.97" x2="42.545" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-13.97" x2="41.91" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-12.065" x2="43.815" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="42.545" y1="-11.43" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="41.91" y1="-13.335" x2="41.91" y2="-12.065" width="0.2032" layer="21"/>
<pad name="40" x="43.18" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="39" x="43.18" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="38" x="43.18" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="37" x="43.18" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="36" x="43.18" y="0" drill="1.016" diameter="1.8796"/>
<pad name="35" x="43.18" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="34" x="43.18" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="33" x="43.18" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="32" x="43.18" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="31" x="43.18" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="42.926" y1="-5.334" x2="43.434" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-2.794" x2="43.434" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-0.254" x2="43.434" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="2.286" x2="43.434" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="4.826" x2="43.434" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="7.366" x2="43.434" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="9.906" x2="43.434" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-7.874" x2="43.434" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-10.414" x2="43.434" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="42.926" y1="-12.954" x2="43.434" y2="-12.446" layer="51" rot="R270"/>
<wire x1="42.545" y1="8.89" x2="41.91" y2="8.89" width="0.2286" layer="21"/>
<wire x1="41.91" y1="8.89" x2="41.91" y2="11.43" width="0.2286" layer="21"/>
<wire x1="41.91" y1="11.43" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="43.815" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.99" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-5.715" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-1.27" x2="46.99" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-1.905" x2="46.99" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-3.175" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-3.175" x2="44.45" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-1.905" x2="45.085" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="46.355" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-3.81" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-5.715" x2="44.45" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="1.27" x2="44.45" y2="1.905" width="0.2032" layer="21"/>
<wire x1="46.355" y1="1.27" x2="46.99" y2="0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="0.635" x2="46.99" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-0.635" x2="46.355" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-1.27" x2="44.45" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-0.635" x2="44.45" y2="0.635" width="0.2032" layer="21"/>
<wire x1="44.45" y1="0.635" x2="45.085" y2="1.27" width="0.2032" layer="21"/>
<wire x1="46.355" y1="6.35" x2="46.99" y2="5.715" width="0.2032" layer="21"/>
<wire x1="46.99" y1="5.715" x2="46.99" y2="4.445" width="0.2032" layer="21"/>
<wire x1="46.99" y1="4.445" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="4.445" width="0.2032" layer="21"/>
<wire x1="44.45" y1="4.445" x2="44.45" y2="5.715" width="0.2032" layer="21"/>
<wire x1="44.45" y1="5.715" x2="45.085" y2="6.35" width="0.2032" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.355" y2="3.81" width="0.2032" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="44.45" y1="1.905" x2="44.45" y2="3.175" width="0.2032" layer="21"/>
<wire x1="46.355" y1="8.89" x2="46.99" y2="8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="8.255" x2="46.99" y2="6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="6.985" x2="46.355" y2="6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="6.35" x2="44.45" y2="6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="6.985" x2="44.45" y2="8.255" width="0.2032" layer="21"/>
<wire x1="44.45" y1="8.255" x2="45.085" y2="8.89" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.99" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-8.255" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="46.355" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-8.255" x2="44.45" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.99" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-10.795" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-9.525" x2="46.355" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-8.89" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-10.795" x2="44.45" y2="-9.525" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.99" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-13.335" x2="46.355" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="46.355" y1="-13.97" x2="45.085" y2="-13.97" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-13.97" x2="44.45" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="46.99" y1="-12.065" x2="46.355" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="45.085" y1="-11.43" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<wire x1="44.45" y1="-13.335" x2="44.45" y2="-12.065" width="0.2032" layer="21"/>
<pad name="20" x="45.72" y="10.16" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="19" x="45.72" y="7.62" drill="1.016" diameter="1.8796"/>
<pad name="18" x="45.72" y="5.08" drill="1.016" diameter="1.8796"/>
<pad name="17" x="45.72" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="16" x="45.72" y="0" drill="1.016" diameter="1.8796"/>
<pad name="15" x="45.72" y="-2.54" drill="1.016" diameter="1.8796"/>
<pad name="14" x="45.72" y="-5.08" drill="1.016" diameter="1.8796"/>
<pad name="13" x="45.72" y="-7.62" drill="1.016" diameter="1.8796"/>
<pad name="12" x="45.72" y="-10.16" drill="1.016" diameter="1.8796"/>
<pad name="11" x="45.72" y="-12.7" drill="1.016" diameter="1.8796"/>
<rectangle x1="45.466" y1="-5.334" x2="45.974" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-2.794" x2="45.974" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-0.254" x2="45.974" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="2.286" x2="45.974" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="4.826" x2="45.974" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="7.366" x2="45.974" y2="7.874" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="9.906" x2="45.974" y2="10.414" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-7.874" x2="45.974" y2="-7.366" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-10.414" x2="45.974" y2="-9.906" layer="51" rot="R270"/>
<rectangle x1="45.466" y1="-12.954" x2="45.974" y2="-12.446" layer="51" rot="R270"/>
<wire x1="45.085" y1="8.89" x2="44.45" y2="8.89" width="0.2286" layer="21"/>
<wire x1="44.45" y1="8.89" x2="44.45" y2="11.43" width="0.2286" layer="21"/>
<wire x1="44.45" y1="11.43" x2="46.99" y2="11.43" width="0.2286" layer="21"/>
<wire x1="46.99" y1="11.43" x2="46.99" y2="8.89" width="0.2286" layer="21"/>
<wire x1="46.99" y1="8.89" x2="46.355" y2="8.89" width="0.2286" layer="21"/>
<text x="7.62" y="9.652" size="0.889" layer="21" font="vector">5V</text>
<text x="7.62" y="7.112" size="0.889" layer="21" font="vector">GND</text>
<text x="7.62" y="4.572" size="0.889" layer="21" font="vector">AA7</text>
<text x="7.62" y="-10.668" size="0.889" layer="21" font="vector">AA1</text>
<text x="-0.254" y="11.938" size="0.889" layer="21" font="vector">1</text>
<text x="3.556" y="11.938" size="0.889" layer="21" font="vector" align="bottom-right">21</text>
<text x="-0.762" y="-15.24" size="0.889" layer="21" font="vector">10</text>
<text x="3.556" y="-15.24" size="0.889" layer="21" font="vector" align="bottom-right">30</text>
<text x="7.62" y="2.032" size="0.889" layer="21" font="vector">AB1</text>
<text x="7.62" y="-0.508" size="0.889" layer="21" font="vector">AA2</text>
<text x="7.62" y="-3.048" size="0.889" layer="21" font="vector">AB2</text>
<text x="7.62" y="-5.588" size="0.889" layer="21" font="vector">AA0</text>
<text x="7.62" y="-8.128" size="0.889" layer="21" font="vector">AB0</text>
<text x="42.418" y="11.938" size="0.889" layer="21" font="vector">40</text>
<text x="46.482" y="11.938" size="0.889" layer="21" font="vector" align="bottom-right">20</text>
<text x="42.672" y="-15.24" size="0.889" layer="21" font="vector">31</text>
<text x="46.736" y="-15.24" size="0.889" layer="21" font="vector" align="bottom-right">11</text>
<text x="37.338" y="7.112" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="4.572" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="2.032" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-0.508" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-3.048" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="37.338" y="-5.588" size="0.889" layer="21" font="vector" align="bottom-right">P13</text>
<text x="37.338" y="-10.668" size="0.889" layer="21" font="vector" align="bottom-right">DAC</text>
<text x="37.338" y="-13.208" size="0.889" layer="21" font="vector" align="bottom-right">DAC</text>
<text x="41.148" y="9.652" size="0.889" layer="21" font="vector" align="bottom-right">GND</text>
<text x="41.148" y="7.112" size="0.889" layer="21" font="vector" align="bottom-right">P19</text>
<text x="41.148" y="4.572" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-10.668" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-0.508" size="0.889" layer="21" font="vector" align="bottom-right">RST</text>
<text x="41.148" y="-3.048" size="0.889" layer="21" font="vector" align="bottom-right">MOSI</text>
<text x="41.148" y="-5.588" size="0.889" layer="21" font="vector" align="bottom-right">MISO</text>
<text x="41.148" y="-8.128" size="0.889" layer="21" font="vector" align="bottom-right">CS</text>
<text x="41.148" y="-13.208" size="0.889" layer="21" font="vector" align="bottom-right">P55</text>
<text x="37.338" y="9.652" size="0.889" layer="21" font="vector" align="bottom-right">PWM</text>
<text x="4.572" y="9.652" size="0.889" layer="21" font="vector">3V3</text>
<text x="4.572" y="7.112" size="0.889" layer="21" font="vector">AA6</text>
<text x="4.572" y="4.572" size="0.889" layer="21" font="vector">RX</text>
<text x="4.572" y="2.032" size="0.889" layer="21" font="vector">TX</text>
<text x="4.572" y="-0.508" size="0.889" layer="21" font="vector">P12</text>
<text x="4.572" y="-3.048" size="0.889" layer="21" font="vector">AB6</text>
<text x="4.572" y="-5.588" size="0.889" layer="21" font="vector">CLK</text>
<text x="4.572" y="-8.128" size="0.889" layer="21" font="vector">P22</text>
<text x="4.572" y="-10.668" size="0.889" layer="21" font="vector">SCL</text>
<text x="4.572" y="-13.208" size="0.889" layer="21" font="vector">SDA</text>
<wire x1="7.112" y1="10.668" x2="7.112" y2="-13.208" width="0.0508" layer="21"/>
<wire x1="37.846" y1="10.668" x2="37.846" y2="-13.208" width="0.0508" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="HEADER4X10-BOOSTERPACK">
<wire x1="-2.54" y1="-12.7" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="15.24" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="15.24" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$1" x="5.08" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$2" x="5.08" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$3" x="5.08" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$4" x="5.08" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$5" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$6" x="5.08" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$7" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$8" x="5.08" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$9" x="5.08" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$10" x="5.08" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="53.34" y1="-12.7" x2="53.34" y2="15.24" width="0.254" layer="94"/>
<wire x1="53.34" y1="15.24" x2="58.42" y2="15.24" width="0.254" layer="94"/>
<wire x1="58.42" y1="15.24" x2="58.42" y2="-12.7" width="0.254" layer="94"/>
<wire x1="58.42" y1="-12.7" x2="53.34" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$20" x="60.96" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$19" x="60.96" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$18" x="60.96" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$17" x="60.96" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$16" x="60.96" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$15" x="60.96" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$14" x="60.96" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$13" x="60.96" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$12" x="60.96" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$11" x="60.96" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="17.78" y2="-12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$21" x="20.32" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$22" x="20.32" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$23" x="20.32" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$24" x="20.32" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$25" x="20.32" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$26" x="20.32" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$27" x="20.32" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$28" x="20.32" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$29" x="20.32" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$30" x="20.32" y="-10.16" visible="pad" length="short" rot="R180"/>
<wire x1="38.1" y1="-12.7" x2="38.1" y2="15.24" width="0.254" layer="94"/>
<wire x1="38.1" y1="15.24" x2="43.18" y2="15.24" width="0.254" layer="94"/>
<wire x1="43.18" y1="15.24" x2="43.18" y2="-12.7" width="0.254" layer="94"/>
<wire x1="43.18" y1="-12.7" x2="38.1" y2="-12.7" width="0.254" layer="94"/>
<pin name="P$40" x="45.72" y="12.7" visible="pad" length="short" rot="R180"/>
<pin name="P$39" x="45.72" y="10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$38" x="45.72" y="7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$37" x="45.72" y="5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$36" x="45.72" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$35" x="45.72" y="0" visible="pad" length="short" rot="R180"/>
<pin name="P$34" x="45.72" y="-2.54" visible="pad" length="short" rot="R180"/>
<pin name="P$33" x="45.72" y="-5.08" visible="pad" length="short" rot="R180"/>
<pin name="P$32" x="45.72" y="-7.62" visible="pad" length="short" rot="R180"/>
<pin name="P$31" x="45.72" y="-10.16" visible="pad" length="short" rot="R180"/>
<text x="-2.54" y="17.78" size="2.54" layer="95" font="vector">J1</text>
<text x="53.34" y="17.78" size="2.54" layer="95" font="vector">J2</text>
<text x="12.7" y="17.78" size="2.54" layer="95" font="vector">J3</text>
<text x="38.1" y="17.78" size="2.54" layer="95" font="vector">J4</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="HEADER4X10-BOOSTERPACK" prefix="A" uservalue="yes">
<gates>
<gate name="G$1" symbol="HEADER4X10-BOOSTERPACK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4X10-BOOSTERPACK">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$11" pad="11"/>
<connect gate="G$1" pin="P$12" pad="12"/>
<connect gate="G$1" pin="P$13" pad="13"/>
<connect gate="G$1" pin="P$14" pad="14"/>
<connect gate="G$1" pin="P$15" pad="15"/>
<connect gate="G$1" pin="P$16" pad="16"/>
<connect gate="G$1" pin="P$17" pad="17"/>
<connect gate="G$1" pin="P$18" pad="18"/>
<connect gate="G$1" pin="P$19" pad="19"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$20" pad="20"/>
<connect gate="G$1" pin="P$21" pad="21"/>
<connect gate="G$1" pin="P$22" pad="22"/>
<connect gate="G$1" pin="P$23" pad="23"/>
<connect gate="G$1" pin="P$24" pad="24"/>
<connect gate="G$1" pin="P$25" pad="25"/>
<connect gate="G$1" pin="P$26" pad="26"/>
<connect gate="G$1" pin="P$27" pad="27"/>
<connect gate="G$1" pin="P$28" pad="28"/>
<connect gate="G$1" pin="P$29" pad="29"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$30" pad="30"/>
<connect gate="G$1" pin="P$31" pad="31"/>
<connect gate="G$1" pin="P$32" pad="32"/>
<connect gate="G$1" pin="P$33" pad="33"/>
<connect gate="G$1" pin="P$34" pad="34"/>
<connect gate="G$1" pin="P$35" pad="35"/>
<connect gate="G$1" pin="P$36" pad="36"/>
<connect gate="G$1" pin="P$37" pad="37"/>
<connect gate="G$1" pin="P$38" pad="38"/>
<connect gate="G$1" pin="P$39" pad="39"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$40" pad="40"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper" urn="urn:adsk.eagle:library:252">
<description>&lt;b&gt;Jumpers&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SJ_2W" urn="urn:adsk.eagle:footprint:15438/1" library_version="1">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="3.175" y1="-1.524" x2="-3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.524" x2="3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.429" y1="1.27" x2="-3.175" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.429" y1="-1.27" x2="-3.175" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="3.175" y1="-1.524" x2="3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="3.429" y1="-1.27" x2="3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.524" x2="3.175" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="0" x2="-3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="0.762" x2="0" y2="1.397" width="0.1524" layer="51"/>
<wire x1="0" y1="-1.397" x2="0" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="2.032" y1="0.127" x2="2.032" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="-2.032" y1="-0.127" x2="-2.032" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-2.54" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="0" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="3" x="2.54" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-3.429" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.1001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.762" x2="0.508" y2="0.762" layer="51"/>
</package>
<package name="SJ_2" urn="urn:adsk.eagle:footprint:15439/1" library_version="1">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="2.159" y1="-1.016" x2="-2.159" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="0.762" x2="-2.159" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="-0.762" x2="-2.159" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.016" x2="2.413" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.762" x2="-2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.016" x2="2.159" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0" x2="-2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="0.762" x2="0" y2="1.016" width="0.1524" layer="51"/>
<wire x1="0" y1="-1.016" x2="0" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.016" y1="0.127" x2="1.016" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="-1.016" y1="-0.127" x2="-1.016" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-1.524" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="3" x="1.524" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-2.413" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.1001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.762" x2="0.508" y2="0.762" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SJ_2W" urn="urn:adsk.eagle:package:15476/1" type="box" library_version="1">
<description>Solder jumper</description>
</package3d>
<package3d name="SJ_2" urn="urn:adsk.eagle:package:15474/1" type="box" library_version="1">
<description>Solder jumper</description>
</package3d>
</packages3d>
<symbols>
<symbol name="SJ_2" urn="urn:adsk.eagle:symbol:15437/1" library_version="1">
<wire x1="-0.635" y1="-1.397" x2="0.635" y2="-1.397" width="1.27" layer="94" curve="180" cap="flat"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<text x="2.54" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.635" x2="1.27" y2="0.635" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SJ2W" urn="urn:adsk.eagle:component:15505/1" prefix="SJ" uservalue="yes" library_version="1">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="SJ_2" x="0" y="0"/>
</gates>
<devices>
<device name="W" package="SJ_2W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15476/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SJ_2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15474/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4-SMALL-DOCFIELD" device="" value="   "/>
<part name="X1" library="MainBoard" deviceset="ESP-12" device="" value="ESP12"/>
<part name="A1" library="motor_driver" deviceset="HEADER4X10-BOOSTERPACK-BOTTOM" device="" value="  "/>
<part name="A2" library="EAGLE_BoosterPack_Library" deviceset="HEADER4X10-BOOSTERPACK" device="" value=" "/>
<part name="R1" library="MainBoard" deviceset="RES" device="C" value="220"/>
<part name="R2" library="MainBoard" deviceset="RES" device="C" value="220"/>
<part name="R3" library="MainBoard" deviceset="RES" device="C" value="220"/>
<part name="T1" library="MainBoard" deviceset="NPNSOT23" device="" value="BJT1"/>
<part name="T2" library="MainBoard" deviceset="NPNSOT23" device="" value="BJT2"/>
<part name="T3" library="MainBoard" deviceset="NPNSOT23" device="" value="BJT3"/>
<part name="R4" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="R5" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="R6" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="SPI" library="MainBoard" deviceset="DIP-HEADER-6P" device="" value="6p-2.54"/>
<part name="I2C" library="MainBoard" deviceset="DIP-HEADER-4P" device="A"/>
<part name="BUZZ" library="MainBoard" deviceset="DIP-BUZZER" device="" value="KS12"/>
<part name="R7" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="T4" library="MainBoard" deviceset="NPNSOT23" device="" value="BJT4"/>
<part name="R8" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="B1" library="MainBoard" deviceset="DIP-BUTTON-4P" device="" value="TS-1109"/>
<part name="B2" library="MainBoard" deviceset="DIP-BUTTON-4P" device="" value="TS-1109"/>
<part name="B3" library="MainBoard" deviceset="DIP-BUTTON-4P" device="" value="TS-1109"/>
<part name="B4" library="MainBoard" deviceset="DIP-BUTTON-4P" device="" value="TS-1109"/>
<part name="R9" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="R10" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="R11" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="R12" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="U1" library="MainBoard" deviceset="9D0F-LSM9DS0" device="A" value="LSM9DS0"/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="FTDI" library="MainBoard" deviceset="DIP-HEADER-6P" device="" value="6p-2.54"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY9" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY10" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY12" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C1" library="MainBoard" deviceset="CAP" device="B" value="10uF"/>
<part name="SUPPLY13" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY14" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R13" library="MainBoard" deviceset="RES" device="C" value="1K"/>
<part name="R14" library="MainBoard" deviceset="RES" device="C" value="10K"/>
<part name="R15" library="MainBoard" deviceset="RES" device="C" value="10K"/>
<part name="R16" library="MainBoard" deviceset="RES" device="C" value="10K"/>
<part name="R17" library="MainBoard" deviceset="RES" device="C" value="10K"/>
<part name="SUPPLY15" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY16" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY17" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY18" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="D1" library="MainBoard" deviceset="LED-SMD" device="B" value="R"/>
<part name="D2" library="MainBoard" deviceset="LED-SMD" device="B" value="Y"/>
<part name="D3" library="MainBoard" deviceset="LED-SMD" device="B" value="G"/>
<part name="B5" library="MainBoard" deviceset="BUTTON_SMD" device="A" value="RESET"/>
<part name="B6" library="MainBoard" deviceset="BUTTON_SMD" device="A" value="BOOT"/>
<part name="SUPPLY19" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY20" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U2" library="MainBoard" deviceset="EEPROM-256K" device="" value="24LC256"/>
<part name="SUPPLY21" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="WP" library="jumper" library_urn="urn:adsk.eagle:library:252" deviceset="SJ2W" device="" package3d_urn="urn:adsk.eagle:package:15474/1" value="3"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="3.81" y1="257.81" x2="176.53" y2="257.81" width="0.1524" layer="97"/>
<wire x1="176.53" y1="257.81" x2="176.53" y2="195.58" width="0.1524" layer="97"/>
<wire x1="176.53" y1="195.58" x2="3.81" y2="195.58" width="0.1524" layer="97"/>
<wire x1="3.81" y1="195.58" x2="3.81" y2="257.81" width="0.1524" layer="97"/>
<text x="146.05" y="196.85" size="1.778" layer="97">Sensor, Wifi and EEPROM</text>
<wire x1="3.81" y1="191.77" x2="176.53" y2="191.77" width="0.1524" layer="97"/>
<wire x1="176.53" y1="191.77" x2="176.53" y2="133.35" width="0.1524" layer="97"/>
<wire x1="176.53" y1="133.35" x2="3.81" y2="133.35" width="0.1524" layer="97"/>
<wire x1="3.81" y1="133.35" x2="3.81" y2="191.77" width="0.1524" layer="97"/>
<text x="146.05" y="134.62" size="1.778" layer="97">Sound and Light Output</text>
<wire x1="3.81" y1="129.54" x2="176.53" y2="129.54" width="0.1524" layer="97"/>
<wire x1="176.53" y1="129.54" x2="176.53" y2="58.42" width="0.1524" layer="97"/>
<wire x1="176.53" y1="58.42" x2="3.81" y2="58.42" width="0.1524" layer="97"/>
<wire x1="3.81" y1="58.42" x2="3.81" y2="129.54" width="0.1524" layer="97"/>
<text x="146.05" y="59.69" size="1.778" layer="97">Buttons and Connectors</text>
<wire x1="3.81" y1="54.61" x2="176.53" y2="54.61" width="0.1524" layer="97"/>
<wire x1="176.53" y1="54.61" x2="176.53" y2="11.43" width="0.1524" layer="97"/>
<wire x1="176.53" y1="11.43" x2="3.81" y2="11.43" width="0.1524" layer="97"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="54.61" width="0.1524" layer="97"/>
<text x="153.67" y="12.7" size="1.778" layer="97">MCU Connector</text>
</plain>
<instances>
<instance part="FRAME1" gate="/1" x="0" y="0"/>
<instance part="X1" gate="G$1" x="40.64" y="231.14"/>
<instance part="A1" gate="G$1" x="100.33" y="30.48"/>
<instance part="A2" gate="G$1" x="10.16" y="30.48"/>
<instance part="R1" gate="G$1" x="91.44" y="176.53" smashed="yes" rot="R270">
<attribute name="NAME" x="92.71" y="178.0286" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.71" y="174.498" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="121.92" y="176.53" smashed="yes" rot="R270">
<attribute name="NAME" x="123.19" y="178.0286" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.19" y="174.498" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="149.86" y="176.53" smashed="yes" rot="R270">
<attribute name="NAME" x="151.13" y="178.0286" size="1.778" layer="95"/>
<attribute name="VALUE" x="151.13" y="174.498" size="1.778" layer="96"/>
</instance>
<instance part="T1" gate="A" x="88.9" y="151.13" smashed="yes">
<attribute name="NAME" x="86.36" y="153.67" size="1.778" layer="95"/>
</instance>
<instance part="T2" gate="A" x="119.38" y="151.13" smashed="yes">
<attribute name="NAME" x="116.84" y="153.67" size="1.778" layer="95"/>
</instance>
<instance part="T3" gate="A" x="147.32" y="151.13" smashed="yes">
<attribute name="NAME" x="144.78" y="153.67" size="1.778" layer="95"/>
</instance>
<instance part="R4" gate="G$1" x="78.74" y="151.13"/>
<instance part="R5" gate="G$1" x="109.22" y="151.13"/>
<instance part="R6" gate="G$1" x="137.16" y="151.13"/>
<instance part="SPI" gate="G$1" x="158.75" y="90.17" smashed="yes">
<attribute name="NAME" x="158.75" y="99.06" size="1.27" layer="95" ratio="10"/>
</instance>
<instance part="I2C" gate="G$1" x="158.75" y="72.39" smashed="yes">
<attribute name="NAME" x="158.75" y="78.74" size="1.27" layer="95" ratio="10"/>
</instance>
<instance part="BUZZ" gate="G$1" x="41.91" y="173.99" smashed="yes" rot="R270">
<attribute name="NAME" x="44.45" y="177.8" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="44.45" y="168.91" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R7" gate="G$1" x="31.75" y="173.99" smashed="yes" rot="R270">
<attribute name="NAME" x="26.67" y="174.2186" size="1.778" layer="95"/>
<attribute name="VALUE" x="26.67" y="170.688" size="1.778" layer="96"/>
</instance>
<instance part="T4" gate="A" x="34.29" y="151.13" smashed="yes">
<attribute name="NAME" x="31.75" y="153.67" size="1.778" layer="95"/>
</instance>
<instance part="R8" gate="G$1" x="24.13" y="151.13" smashed="yes">
<attribute name="NAME" x="20.32" y="152.6286" size="1.778" layer="95"/>
<attribute name="VALUE" x="20.32" y="147.828" size="1.778" layer="96"/>
</instance>
<instance part="B1" gate="G$1" x="24.13" y="120.65" smashed="yes">
<attribute name="NAME" x="20.32" y="127" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="19.05" y="113.03" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="B2" gate="G$1" x="60.96" y="120.65" smashed="yes">
<attribute name="NAME" x="57.15" y="127" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="55.88" y="113.03" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="B3" gate="G$1" x="24.13" y="86.36" smashed="yes">
<attribute name="NAME" x="20.32" y="92.71" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="19.05" y="78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="B4" gate="G$1" x="60.96" y="86.36" smashed="yes">
<attribute name="NAME" x="57.15" y="92.71" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="55.88" y="78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R9" gate="G$1" x="35.56" y="110.49" smashed="yes" rot="R90">
<attribute name="NAME" x="36.83" y="111.9886" size="1.778" layer="95"/>
<attribute name="VALUE" x="36.83" y="108.458" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="73.66" y="110.49" smashed="yes" rot="R90">
<attribute name="NAME" x="74.93" y="111.9886" size="1.778" layer="95"/>
<attribute name="VALUE" x="74.93" y="108.458" size="1.778" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="35.56" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="36.83" y="77.6986" size="1.778" layer="95"/>
<attribute name="VALUE" x="36.83" y="74.168" size="1.778" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="73.66" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="74.93" y="77.6986" size="1.778" layer="95"/>
<attribute name="VALUE" x="74.93" y="74.168" size="1.778" layer="96"/>
</instance>
<instance part="U1" gate="G$1" x="105.41" y="228.6"/>
<instance part="SUPPLY4" gate="GND" x="91.44" y="140.97"/>
<instance part="SUPPLY5" gate="GND" x="121.92" y="140.97"/>
<instance part="SUPPLY6" gate="GND" x="149.86" y="140.97"/>
<instance part="SUPPLY7" gate="GND" x="100.33" y="203.2"/>
<instance part="FTDI" gate="G$1" x="158.75" y="118.11" smashed="yes">
<attribute name="NAME" x="157.48" y="127" size="1.27" layer="95" ratio="10"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="36.83" y="140.97"/>
<instance part="SUPPLY2" gate="GND" x="35.56" y="100.33"/>
<instance part="SUPPLY3" gate="GND" x="73.66" y="100.33"/>
<instance part="SUPPLY8" gate="GND" x="35.56" y="66.04"/>
<instance part="SUPPLY9" gate="GND" x="73.66" y="66.04"/>
<instance part="SUPPLY10" gate="GND" x="139.7" y="68.58"/>
<instance part="SUPPLY11" gate="GND" x="139.7" y="86.36"/>
<instance part="SUPPLY12" gate="GND" x="63.5" y="203.2"/>
<instance part="C1" gate="G$1" x="16.51" y="213.36" smashed="yes">
<attribute name="NAME" x="18.034" y="213.741" size="1.778" layer="95"/>
<attribute name="VALUE" x="18.034" y="208.661" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY13" gate="GND" x="16.51" y="203.2"/>
<instance part="SUPPLY14" gate="GND" x="148.59" y="106.68"/>
<instance part="R13" gate="G$1" x="72.39" y="233.68" smashed="yes" rot="R90">
<attribute name="NAME" x="73.66" y="235.1786" size="1.778" layer="95"/>
<attribute name="VALUE" x="73.66" y="231.648" size="1.778" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="81.28" y="233.68" smashed="yes" rot="R90">
<attribute name="NAME" x="82.55" y="235.1786" size="1.778" layer="95"/>
<attribute name="VALUE" x="82.55" y="231.648" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="66.04" y="213.36" smashed="yes" rot="R90">
<attribute name="NAME" x="67.31" y="214.8586" size="1.778" layer="95"/>
<attribute name="VALUE" x="67.31" y="211.328" size="1.778" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="20.32" y="243.84" smashed="yes" rot="R90">
<attribute name="NAME" x="21.59" y="245.3386" size="1.778" layer="95"/>
<attribute name="VALUE" x="21.59" y="241.808" size="1.778" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="10.16" y="243.84" smashed="yes" rot="R90">
<attribute name="NAME" x="11.43" y="245.3386" size="1.778" layer="95"/>
<attribute name="VALUE" x="11.43" y="241.808" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY15" gate="GND" x="35.56" y="35.56"/>
<instance part="SUPPLY16" gate="GND" x="80.01" y="36.83"/>
<instance part="SUPPLY17" gate="GND" x="125.73" y="36.83"/>
<instance part="SUPPLY18" gate="GND" x="167.64" y="36.83"/>
<instance part="D1" gate="G$1" x="91.44" y="163.83" smashed="yes" rot="R270">
<attribute name="NAME" x="87.63" y="166.37" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="92.71" y="166.37" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D2" gate="G$1" x="121.92" y="163.83" smashed="yes" rot="R270">
<attribute name="NAME" x="118.11" y="166.37" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="123.19" y="166.37" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D3" gate="G$1" x="149.86" y="163.83" smashed="yes" rot="R270">
<attribute name="NAME" x="147.32" y="166.37" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="151.13" y="166.37" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="B5" gate="G$1" x="107.95" y="120.65" smashed="yes">
<attribute name="NAME" x="102.87" y="127" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="102.87" y="113.03" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="B6" gate="G$1" x="109.22" y="86.36" smashed="yes">
<attribute name="NAME" x="104.14" y="92.71" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="104.14" y="78.74" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY19" gate="GND" x="121.92" y="111.76"/>
<instance part="SUPPLY20" gate="GND" x="123.19" y="77.47"/>
<instance part="U2" gate="G$1" x="142.24" y="231.14"/>
<instance part="SUPPLY21" gate="GND" x="127" y="203.2"/>
<instance part="WP" gate="G$1" x="170.18" y="231.14" smashed="yes">
<attribute name="NAME" x="171.45" y="234.061" size="1.778" layer="95"/>
<attribute name="VALUE" x="172.72" y="229.235" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="91.44" y1="171.45" x2="91.44" y2="167.64" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="T1" gate="A" pin="C"/>
<wire x1="91.44" y1="160.02" x2="91.44" y2="156.21" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="-"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="T1" gate="A" pin="E"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="91.44" y1="146.05" x2="91.44" y2="143.51" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T3" gate="A" pin="E"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="149.86" y1="146.05" x2="149.86" y2="143.51" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="A" pin="E"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="121.92" y1="146.05" x2="121.92" y2="143.51" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T4" gate="A" pin="E"/>
<wire x1="36.83" y1="146.05" x2="36.83" y2="143.51" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="100.33" y1="214.63" x2="100.33" y2="205.74" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="35.56" y1="105.41" x2="35.56" y2="102.87" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="73.66" y1="105.41" x2="73.66" y2="102.87" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="35.56" y1="71.12" x2="35.56" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="73.66" y1="71.12" x2="73.66" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C" gate="G$1" pin="P$2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="151.13" y1="73.66" x2="139.7" y2="73.66" width="0.1524" layer="91"/>
<wire x1="139.7" y1="73.66" x2="139.7" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SPI" gate="G$1" pin="2"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="151.13" y1="93.98" x2="139.7" y2="93.98" width="0.1524" layer="91"/>
<wire x1="139.7" y1="93.98" x2="139.7" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="16.51" y1="205.74" x2="16.51" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="148.59" y1="109.22" x2="148.59" y2="111.76" width="0.1524" layer="91"/>
<pinref part="FTDI" gate="G$1" pin="6"/>
<wire x1="148.59" y1="111.76" x2="151.13" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="58.42" y1="218.44" x2="60.96" y2="218.44" width="0.1524" layer="91"/>
<wire x1="60.96" y1="218.44" x2="60.96" y2="207.01" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="66.04" y1="208.28" x2="66.04" y2="207.01" width="0.1524" layer="91"/>
<wire x1="66.04" y1="207.01" x2="63.5" y2="207.01" width="0.1524" layer="91"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="63.5" y1="207.01" x2="60.96" y2="207.01" width="0.1524" layer="91"/>
<wire x1="63.5" y1="205.74" x2="63.5" y2="207.01" width="0.1524" layer="91"/>
<junction x="63.5" y="207.01"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$20"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<wire x1="161.29" y1="43.18" x2="167.64" y2="43.18" width="0.1524" layer="91"/>
<wire x1="167.64" y1="43.18" x2="167.64" y2="39.37" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$22"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<wire x1="120.65" y1="40.64" x2="125.73" y2="40.64" width="0.1524" layer="91"/>
<wire x1="125.73" y1="40.64" x2="125.73" y2="39.37" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$20"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="71.12" y1="43.18" x2="80.01" y2="43.18" width="0.1524" layer="91"/>
<wire x1="80.01" y1="43.18" x2="80.01" y2="39.37" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$22"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="30.48" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<wire x1="35.56" y1="40.64" x2="35.56" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="B5" gate="G$1" pin="B1"/>
<wire x1="116.84" y1="118.11" x2="121.92" y2="118.11" width="0.1524" layer="91"/>
<wire x1="121.92" y1="118.11" x2="121.92" y2="114.3" width="0.1524" layer="91"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="B6" gate="G$1" pin="B1"/>
<wire x1="118.11" y1="83.82" x2="123.19" y2="83.82" width="0.1524" layer="91"/>
<wire x1="123.19" y1="83.82" x2="123.19" y2="80.01" width="0.1524" layer="91"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="A0"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<wire x1="129.54" y1="233.68" x2="127" y2="233.68" width="0.1524" layer="91"/>
<wire x1="127" y1="233.68" x2="127" y2="231.14" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="A1"/>
<wire x1="127" y1="231.14" x2="127" y2="228.6" width="0.1524" layer="91"/>
<wire x1="127" y1="228.6" x2="127" y2="226.06" width="0.1524" layer="91"/>
<wire x1="127" y1="226.06" x2="127" y2="218.44" width="0.1524" layer="91"/>
<wire x1="127" y1="218.44" x2="127" y2="205.74" width="0.1524" layer="91"/>
<wire x1="129.54" y1="231.14" x2="127" y2="231.14" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="A2"/>
<wire x1="129.54" y1="228.6" x2="127" y2="228.6" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VSS"/>
<wire x1="129.54" y1="226.06" x2="127" y2="226.06" width="0.1524" layer="91"/>
<junction x="127" y="226.06"/>
<junction x="127" y="228.6"/>
<junction x="127" y="231.14"/>
<pinref part="WP" gate="G$1" pin="3"/>
<wire x1="170.18" y1="226.06" x2="170.18" y2="218.44" width="0.1524" layer="91"/>
<wire x1="170.18" y1="218.44" x2="127" y2="218.44" width="0.1524" layer="91"/>
<junction x="127" y="218.44"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="T1" gate="A" pin="B"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="86.36" y1="151.13" x2="83.82" y2="151.13" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="T2" gate="A" pin="B"/>
<wire x1="114.3" y1="151.13" x2="116.84" y2="151.13" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="T2" gate="A" pin="C"/>
<wire x1="121.92" y1="156.21" x2="121.92" y2="160.02" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="-"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="121.92" y1="167.64" x2="121.92" y2="171.45" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="149.86" y1="171.45" x2="149.86" y2="167.64" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="T3" gate="A" pin="C"/>
<wire x1="149.86" y1="160.02" x2="149.86" y2="156.21" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="-"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="T3" gate="A" pin="B"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="144.78" y1="151.13" x2="142.24" y2="151.13" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LG" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="132.08" y1="151.13" x2="129.54" y2="151.13" width="0.1524" layer="91"/>
<label x="129.54" y="151.13" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$39"/>
<wire x1="55.88" y1="40.64" x2="57.15" y2="40.64" width="0.1524" layer="91"/>
<label x="57.15" y="40.64" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="LY" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="104.14" y1="151.13" x2="101.6" y2="151.13" width="0.1524" layer="91"/>
<label x="101.6" y="151.13" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$38"/>
<wire x1="55.88" y1="38.1" x2="57.15" y2="38.1" width="0.1524" layer="91"/>
<label x="57.15" y="38.1" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="LR" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="73.66" y1="151.13" x2="71.12" y2="151.13" width="0.1524" layer="91"/>
<label x="71.12" y="151.13" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$37"/>
<wire x1="55.88" y1="35.56" x2="57.15" y2="35.56" width="0.1524" layer="91"/>
<label x="57.15" y="35.56" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="91.44" y1="181.61" x2="91.44" y2="184.15" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="121.92" y1="181.61" x2="121.92" y2="184.15" width="0.1524" layer="91"/>
<wire x1="121.92" y1="184.15" x2="91.44" y2="184.15" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="149.86" y1="181.61" x2="149.86" y2="184.15" width="0.1524" layer="91"/>
<wire x1="149.86" y1="184.15" x2="121.92" y2="184.15" width="0.1524" layer="91"/>
<wire x1="121.92" y1="184.15" x2="121.92" y2="186.69" width="0.1524" layer="91"/>
<junction x="121.92" y="184.15"/>
<label x="121.92" y="186.69" size="0.762" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$21"/>
<wire x1="30.48" y1="43.18" x2="31.75" y2="43.18" width="0.1524" layer="91"/>
<label x="31.75" y="43.18" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="BUZZ" gate="G$1" pin="-"/>
<wire x1="41.91" y1="166.37" x2="41.91" y2="163.83" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="41.91" y1="163.83" x2="36.83" y2="163.83" width="0.1524" layer="91"/>
<wire x1="36.83" y1="163.83" x2="31.75" y2="163.83" width="0.1524" layer="91"/>
<wire x1="31.75" y1="163.83" x2="31.75" y2="168.91" width="0.1524" layer="91"/>
<pinref part="T4" gate="A" pin="C"/>
<wire x1="36.83" y1="156.21" x2="36.83" y2="163.83" width="0.1524" layer="91"/>
<junction x="36.83" y="163.83"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="T4" gate="A" pin="B"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="31.75" y1="151.13" x2="29.21" y2="151.13" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BZ" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="19.05" y1="151.13" x2="16.51" y2="151.13" width="0.1524" layer="91"/>
<label x="16.51" y="151.13" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$40"/>
<wire x1="55.88" y1="43.18" x2="57.15" y2="43.18" width="0.1524" layer="91"/>
<label x="57.15" y="43.18" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCL"/>
<wire x1="102.87" y1="214.63" x2="102.87" y2="210.82" width="0.1524" layer="91"/>
<label x="102.87" y="210.82" size="0.762" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="I2C" gate="G$1" pin="P$3"/>
<wire x1="151.13" y1="71.12" x2="148.59" y2="71.12" width="0.1524" layer="91"/>
<label x="148.59" y="71.12" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$9"/>
<wire x1="15.24" y1="22.86" x2="16.51" y2="22.86" width="0.1524" layer="91"/>
<label x="16.51" y="22.86" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SCL"/>
<wire x1="154.94" y1="228.6" x2="157.48" y2="228.6" width="0.1524" layer="91"/>
<label x="157.48" y="228.6" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SDA"/>
<wire x1="105.41" y1="214.63" x2="105.41" y2="210.82" width="0.1524" layer="91"/>
<label x="105.41" y="210.82" size="0.762" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="I2C" gate="G$1" pin="P$4"/>
<wire x1="151.13" y1="68.58" x2="148.59" y2="68.58" width="0.1524" layer="91"/>
<label x="148.59" y="68.58" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$10"/>
<wire x1="15.24" y1="20.32" x2="16.51" y2="20.32" width="0.1524" layer="91"/>
<label x="16.51" y="20.32" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SDA"/>
<wire x1="154.94" y1="226.06" x2="157.48" y2="226.06" width="0.1524" layer="91"/>
<label x="157.48" y="226.06" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="B1" class="0">
<segment>
<pinref part="B1" gate="G$1" pin="B1"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="33.02" y1="118.11" x2="35.56" y2="118.11" width="0.1524" layer="91"/>
<wire x1="35.56" y1="118.11" x2="35.56" y2="115.57" width="0.1524" layer="91"/>
<wire x1="35.56" y1="118.11" x2="38.1" y2="118.11" width="0.1524" layer="91"/>
<label x="38.1" y="118.11" size="0.762" layer="95" xref="yes"/>
<junction x="35.56" y="118.11"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$18"/>
<wire x1="71.12" y1="38.1" x2="72.39" y2="38.1" width="0.1524" layer="91"/>
<label x="72.39" y="38.1" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="B2" class="0">
<segment>
<pinref part="B2" gate="G$1" pin="B1"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="69.85" y1="118.11" x2="73.66" y2="118.11" width="0.1524" layer="91"/>
<wire x1="73.66" y1="118.11" x2="73.66" y2="115.57" width="0.1524" layer="91"/>
<wire x1="73.66" y1="118.11" x2="76.2" y2="118.11" width="0.1524" layer="91"/>
<label x="76.2" y="118.11" size="0.762" layer="95" xref="yes"/>
<junction x="73.66" y="118.11"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$35"/>
<wire x1="55.88" y1="30.48" x2="57.15" y2="30.48" width="0.1524" layer="91"/>
<label x="57.15" y="30.48" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="B3" class="0">
<segment>
<pinref part="B3" gate="G$1" pin="B1"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="33.02" y1="83.82" x2="35.56" y2="83.82" width="0.1524" layer="91"/>
<wire x1="35.56" y1="83.82" x2="35.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="35.56" y1="83.82" x2="38.1" y2="83.82" width="0.1524" layer="91"/>
<label x="38.1" y="83.82" size="0.762" layer="95" xref="yes"/>
<junction x="35.56" y="83.82"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$36"/>
<wire x1="55.88" y1="33.02" x2="57.15" y2="33.02" width="0.1524" layer="91"/>
<label x="57.15" y="33.02" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="B4" class="0">
<segment>
<pinref part="B4" gate="G$1" pin="B1"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="69.85" y1="83.82" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="73.66" y1="83.82" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="73.66" y1="83.82" x2="76.2" y2="83.82" width="0.1524" layer="91"/>
<label x="76.2" y="83.82" size="0.762" layer="95" xref="yes"/>
<junction x="73.66" y="83.82"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$19"/>
<wire x1="71.12" y1="40.64" x2="72.39" y2="40.64" width="0.1524" layer="91"/>
<label x="72.39" y="40.64" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="B1" gate="G$1" pin="A0"/>
<wire x1="15.24" y1="123.19" x2="12.7" y2="123.19" width="0.1524" layer="91"/>
<label x="12.7" y="123.19" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="B2" gate="G$1" pin="A0"/>
<wire x1="52.07" y1="123.19" x2="49.53" y2="123.19" width="0.1524" layer="91"/>
<label x="49.53" y="123.19" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="B3" gate="G$1" pin="A0"/>
<wire x1="15.24" y1="88.9" x2="12.7" y2="88.9" width="0.1524" layer="91"/>
<label x="12.7" y="88.9" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="B4" gate="G$1" pin="A0"/>
<wire x1="52.07" y1="88.9" x2="49.53" y2="88.9" width="0.1524" layer="91"/>
<label x="49.53" y="88.9" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="31.75" y1="179.07" x2="31.75" y2="184.15" width="0.1524" layer="91"/>
<pinref part="BUZZ" gate="G$1" pin="+"/>
<wire x1="31.75" y1="184.15" x2="36.83" y2="184.15" width="0.1524" layer="91"/>
<wire x1="36.83" y1="184.15" x2="41.91" y2="184.15" width="0.1524" layer="91"/>
<wire x1="41.91" y1="184.15" x2="41.91" y2="181.61" width="0.1524" layer="91"/>
<wire x1="36.83" y1="184.15" x2="36.83" y2="186.69" width="0.1524" layer="91"/>
<junction x="36.83" y="184.15"/>
<label x="36.83" y="186.69" size="0.762" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="I2C" gate="G$1" pin="P$1"/>
<wire x1="151.13" y1="76.2" x2="148.59" y2="76.2" width="0.1524" layer="91"/>
<label x="148.59" y="76.2" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SPI" gate="G$1" pin="1"/>
<wire x1="151.13" y1="96.52" x2="148.59" y2="96.52" width="0.1524" layer="91"/>
<label x="148.59" y="96.52" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VIN"/>
<wire x1="95.25" y1="214.63" x2="95.25" y2="210.82" width="0.1524" layer="91"/>
<label x="95.25" y="210.82" size="0.762" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="VCC"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="16.51" y1="215.9" x2="16.51" y2="218.44" width="0.1524" layer="91"/>
<wire x1="16.51" y1="218.44" x2="22.86" y2="218.44" width="0.1524" layer="91"/>
<wire x1="16.51" y1="218.44" x2="13.97" y2="218.44" width="0.1524" layer="91"/>
<label x="13.97" y="218.44" size="0.762" layer="95" rot="R180" xref="yes"/>
<junction x="16.51" y="218.44"/>
</segment>
<segment>
<pinref part="FTDI" gate="G$1" pin="4"/>
<wire x1="151.13" y1="116.84" x2="148.59" y2="116.84" width="0.1524" layer="91"/>
<label x="148.59" y="116.84" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="72.39" y1="238.76" x2="72.39" y2="241.3" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="81.28" y1="238.76" x2="81.28" y2="241.3" width="0.1524" layer="91"/>
<wire x1="81.28" y1="241.3" x2="76.2" y2="241.3" width="0.1524" layer="91"/>
<wire x1="76.2" y1="241.3" x2="72.39" y2="241.3" width="0.1524" layer="91"/>
<wire x1="76.2" y1="241.3" x2="76.2" y2="243.84" width="0.1524" layer="91"/>
<label x="76.2" y="243.84" size="0.762" layer="95" rot="R90" xref="yes"/>
<junction x="76.2" y="241.3"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="20.32" y1="248.92" x2="20.32" y2="250.19" width="0.1524" layer="91"/>
<label x="20.32" y="252.73" size="0.762" layer="95" rot="R90" xref="yes"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="20.32" y1="250.19" x2="20.32" y2="252.73" width="0.1524" layer="91"/>
<wire x1="10.16" y1="248.92" x2="10.16" y2="250.19" width="0.1524" layer="91"/>
<wire x1="10.16" y1="250.19" x2="20.32" y2="250.19" width="0.1524" layer="91"/>
<junction x="20.32" y="250.19"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$1"/>
<wire x1="15.24" y1="43.18" x2="16.51" y2="43.18" width="0.1524" layer="91"/>
<label x="16.51" y="43.18" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$1"/>
<wire x1="105.41" y1="43.18" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<label x="106.68" y="43.18" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="154.94" y1="233.68" x2="157.48" y2="233.68" width="0.1524" layer="91"/>
<label x="157.48" y="233.68" size="0.762" layer="95" xref="yes"/>
<pinref part="WP" gate="G$1" pin="1"/>
<wire x1="170.18" y1="236.22" x2="170.18" y2="238.76" width="0.1524" layer="91"/>
<wire x1="170.18" y1="238.76" x2="154.94" y2="238.76" width="0.1524" layer="91"/>
<wire x1="154.94" y1="238.76" x2="154.94" y2="233.68" width="0.1524" layer="91"/>
<junction x="154.94" y="233.68"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="SPI" gate="G$1" pin="3"/>
<wire x1="151.13" y1="91.44" x2="148.59" y2="91.44" width="0.1524" layer="91"/>
<label x="148.59" y="91.44" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$7"/>
<wire x1="15.24" y1="27.94" x2="16.51" y2="27.94" width="0.1524" layer="91"/>
<label x="16.51" y="27.94" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="SPI" gate="G$1" pin="4"/>
<wire x1="151.13" y1="88.9" x2="148.59" y2="88.9" width="0.1524" layer="91"/>
<label x="148.59" y="88.9" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$15"/>
<wire x1="71.12" y1="30.48" x2="72.39" y2="30.48" width="0.1524" layer="91"/>
<label x="72.39" y="30.48" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="SPI" gate="G$1" pin="5"/>
<wire x1="151.13" y1="86.36" x2="148.59" y2="86.36" width="0.1524" layer="91"/>
<label x="148.59" y="86.36" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$14"/>
<wire x1="71.12" y1="27.94" x2="72.39" y2="27.94" width="0.1524" layer="91"/>
<label x="72.39" y="27.94" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="SPI" gate="G$1" pin="6"/>
<wire x1="151.13" y1="83.82" x2="148.59" y2="83.82" width="0.1524" layer="91"/>
<label x="148.59" y="83.82" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$12"/>
<wire x1="71.12" y1="22.86" x2="72.39" y2="22.86" width="0.1524" layer="91"/>
<label x="72.39" y="22.86" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="RESET"/>
<wire x1="22.86" y1="236.22" x2="20.32" y2="236.22" width="0.1524" layer="91"/>
<label x="17.78" y="236.22" size="0.762" layer="95" rot="R180" xref="yes"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="20.32" y1="236.22" x2="17.78" y2="236.22" width="0.1524" layer="91"/>
<wire x1="20.32" y1="238.76" x2="20.32" y2="236.22" width="0.1524" layer="91"/>
<junction x="20.32" y="236.22"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GPIO16"/>
<wire x1="22.86" y1="228.6" x2="17.78" y2="228.6" width="0.1524" layer="91"/>
<label x="17.78" y="228.6" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$8"/>
<wire x1="15.24" y1="25.4" x2="16.51" y2="25.4" width="0.1524" layer="91"/>
<label x="16.51" y="25.4" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="B5" gate="G$1" pin="A0"/>
<wire x1="99.06" y1="123.19" x2="95.25" y2="123.19" width="0.1524" layer="91"/>
<label x="95.25" y="123.19" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="TXD"/>
<wire x1="58.42" y1="236.22" x2="62.23" y2="236.22" width="0.1524" layer="91"/>
<label x="62.23" y="236.22" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="FTDI" gate="G$1" pin="2"/>
<wire x1="151.13" y1="121.92" x2="148.59" y2="121.92" width="0.1524" layer="91"/>
<label x="148.59" y="121.92" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$3"/>
<wire x1="105.41" y1="38.1" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<label x="106.68" y="38.1" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="RXD"/>
<wire x1="58.42" y1="233.68" x2="62.23" y2="233.68" width="0.1524" layer="91"/>
<label x="62.23" y="233.68" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="FTDI" gate="G$1" pin="3"/>
<wire x1="151.13" y1="119.38" x2="148.59" y2="119.38" width="0.1524" layer="91"/>
<label x="148.59" y="119.38" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="G$1" pin="P$4"/>
<wire x1="105.41" y1="35.56" x2="106.68" y2="35.56" width="0.1524" layer="91"/>
<label x="106.68" y="35.56" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="GPIO15"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="58.42" y1="220.98" x2="66.04" y2="220.98" width="0.1524" layer="91"/>
<wire x1="66.04" y1="220.98" x2="66.04" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="GPIO2"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="58.42" y1="223.52" x2="81.28" y2="223.52" width="0.1524" layer="91"/>
<wire x1="81.28" y1="223.52" x2="81.28" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BOOT" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="GPIO0"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="58.42" y1="226.06" x2="72.39" y2="226.06" width="0.1524" layer="91"/>
<wire x1="72.39" y1="226.06" x2="72.39" y2="228.6" width="0.1524" layer="91"/>
<wire x1="72.39" y1="226.06" x2="74.93" y2="226.06" width="0.1524" layer="91"/>
<label x="74.93" y="226.06" size="0.762" layer="95" xref="yes"/>
<junction x="72.39" y="226.06"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="P$34"/>
<wire x1="55.88" y1="27.94" x2="57.15" y2="27.94" width="0.1524" layer="91"/>
<label x="57.15" y="27.94" size="0.762" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="B6" gate="G$1" pin="A0"/>
<wire x1="100.33" y1="88.9" x2="95.25" y2="88.9" width="0.1524" layer="91"/>
<label x="95.25" y="88.9" size="0.762" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="CH_PD"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="22.86" y1="231.14" x2="10.16" y2="231.14" width="0.1524" layer="91"/>
<wire x1="10.16" y1="231.14" x2="10.16" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="WP"/>
<pinref part="WP" gate="G$1" pin="2"/>
<wire x1="154.94" y1="231.14" x2="165.1" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,22.86,218.44,X1,VCC,3V3,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
