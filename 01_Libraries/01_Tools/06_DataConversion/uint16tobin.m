function binnum = uint16tobin(uintnum) 
%a = uint16([17145 6291]); % Input uint16 vector (test single:65.2)
t = 16; %input data type length
n = length(uintnum); % number of element in the input vector
binnum = uint8(zeros(1,n*t)); % initialize with zeros solution vector
for ii=1:n
    % Calculate the first bit of the array
    binnum(end-((ii-1)*t)) = bitand(uintnum(n-ii+1),1); 
    for jj=1:(t-1)
            % Calculate the remaining bits for each data type length block
            binnum(end-jj-(ii-1)*t) = bitand(bitshift(uintnum(n-ii+1),-jj),1); 
    end
end
end