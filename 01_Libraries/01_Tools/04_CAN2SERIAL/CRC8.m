function CRC8 = CRC8(bytes,Startvalue)
if isempty(coder.target)
    if ~exist('bytes','var');
        bytes = uint8(170);
        Startvalue = uint8(0);
    end
end
assert(isa(bytes,'uint8'),'Please give uint8 input');
if nargin ~=2
    Startvalue = uint8(0);
end
assert(isa(Startvalue,'uint8'),'Please give uint8 input');


CRCMASK = hex2dec('D5');   %CRC-8 SAE
CRC8 = Startvalue;

for ibyte = 1:length(bytes)
    for ibit = 0:7
        if bitxor(bitand(uint8(128),CRC8),   bitand(uint8(128), bitshift(bytes(ibyte),ibit)))
            CRC8 = bitshift(CRC8,1);
            CRC8 = bitxor(CRC8,CRCMASK);
        else
            CRC8 = bitshift(CRC8,1);
        end
    end
end

