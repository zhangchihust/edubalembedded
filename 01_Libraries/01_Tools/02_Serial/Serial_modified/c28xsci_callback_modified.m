function c28xsci_callback_modified(action)

% Copyright 2006-2010 The MathWorks, Inc.

if (nargin == 0) 
    action = 'noaction'; 
end

blk = gcb;

maskVariables    = get_param(blk, 'Maskvariables'); 
maskVisibilities = get_param(blk, 'MaskVisibilities');
maskDisplay      = get_param(blk, 'MaskDisplay');

switch action
case 'changeErrOption'
    errOption = get_param(blk, 'errOption');
    
    %find the index of mask variable "errValue"
    regexpStr = '(?<=errValue=@)\<[0-9]+\>(?=;)';
    errValueIdx = regexp(maskVariables, regexpStr, 'match');
    errValueIdx = str2num(errValueIdx{1});
    
    if(strcmpi(errOption, 'Output the last received value'))
        maskVisibilities(errValueIdx) = {'off'};
    else
        maskVisibilities(errValueIdx) = {'on'};
    end
    set_param(blk, 'MaskVisibilities', maskVisibilities);

case 'changeIntOption'
    postInt = get_param(blk, 'postInterrupt');
    
    %find the index of mask variable "numFifo"
    regexpStr = '(?<=numFifo=@)\<[0-9]+\>(?=;)';
    numFifoIdx = regexp(maskVariables, regexpStr, 'match');
    numFifoIdx = str2num(numFifoIdx{1});

    if(strcmpi(postInt, 'on'))
        maskVisibilities(numFifoIdx) = {'on'};
    else
        maskVisibilities(numFifoIdx) = {'off'};
    end
    set_param(blk, 'MaskVisibilities', maskVisibilities);
    
case 'renderDisplay'
    outputStatus     = get_param(blk, 'outputStatus');
    portLabelStr   = 'port_label(''output'', 2, ''num'');';
    regexpStr = regexptranslate('escape', portLabelStr);
    [st, en] = regexp(maskDisplay, regexpStr, 'start', 'end');
    
    if(strcmpi(outputStatus, 'on'))
        if(isempty(st))
            maskDisplay = sprintf('%s\n%s', maskDisplay, portLabelStr);
        end
    else
        if(~isempty(st))
            maskDisplay = regexprep(maskDisplay, (['\n' regexpStr]), '');
        end
    end
    
    set_param(blk, 'MaskDisplay', maskDisplay);

end
