%% $RCSfile: c28xsci_rx.tlc,v $
%% $Revision: 1.1.6.3 $ $Date: 2010/05/13 17:46:03 $
%% Copyright 2005-2010 The MathWorks, Inc.

%implements c28xsci_rx_modified "C"

%include "c28xsci_util_modified.tlc"
%include "c28xsci_comm_modified.tlc"

%% Function: BlockTypeSetup ==============================================
%function BlockTypeSetup(block, system) void


    %assign ::SciUtil_modified   = comm_GetSciUtil_modified()

    %<comm_BlockTypeSetup_modified(SciUtil_modified)>
    
    %assign hHdrFile = LibCreateSourceFile("Header", "Custom", SciUtil_modified)
    %<LibSetSourceFileSection(hHdrFile, "Includes", "\n#include <string.h>\n")>

%endfunction %% BlockTypeSetup



%% Function: BlockInstanceSetup ===============================================
%%
%function BlockInstanceSetup(block,system) void
    
    %<comm_AddParamsToBlock_modified(block)>
    %<composeSciUtilHdr_modified(SciUtil_modified)>
 
    %assign fcnbuff = render_scix_rcv_modified("fcn", ModuleL)
    %assign hdrbuff = render_scix_rcv_modified("hdr", ModuleL)
    %<composeSciUtil_modified(SciUtil_modified, fcnbuff, hdrbuff)>
    
%endfunction %% BlockInstanceSetup



%% Function: Start ============================================================
%%
%%
%function Start(block,system) Output

    %assign outportId	 = 0
    %assign outport	 = LibBlockOutputSignal(outportId, "", "", 0)
    %assign outportAddr	 = LibBlockOutputSignalAddr(outportId, "", "", 0)
    %assign outportWidth = LibBlockOutputSignalWidth(outportId)
    %assign outportType  = LibBlockOutputSignalDataTypeName(outportId,"")
    
    /* Initialize %<outport> */
    {
    %foreach k = outportWidth
        %<LibBlockOutputSignal(outportId, "", "", k)> = (%<outportType>)%<InitValue[k]>;
    %endforeach     
    }

%endfunction %% Start



%% Function: Outputs ==========================================================
%%
%%  Read input buffer (when ready), and convert
%%
%function Outputs(block,system) Output  

%%/* %<Type> Block: %<Name> (%<ParamSettings.FunctionName>) */
/* %<Type> Block: %<Name> (S-Function RE) */
{
    %assign outportId    = 0
    %assign outportAddr  = LibBlockOutputSignalAddr(outportId, "", "", 0)
    %assign outportWidth = LibBlockOutputSignalWidth(outportId)
    %assign outportType  = LibBlockOutputSignalDataTypeName(outportId,"")
    %assign typeLen      = 1
    %assign RXERR	 = "RXERR%<ModuleU>"
    
    
	%assign numportId = 1
	%assign numport = LibBlockOutputSignal(numportId, "", "", 0)
    
    
    %assign outportLen = %<outportWidth * typeLen>
	    
    int i;

    char recbuff[%<outportLen>];
    int numberBytes = 0;
    
    %if typeLen >1
	unsigned int outData[%<outportLen/2>];
    %endif

    for(i = 0; i < %<outportLen>; i++) recbuff[i] = 0;

	/* Receiving data */
	numberBytes = sci%<ModuleL>_rcv_modified(recbuff, %<outportLen>);

	    
    %if typeLen > 1
	byteswap_%<ByteOrder[0]>%<SwapWidth>cmp(outData, recbuff, %<outportWidth>, %<typeLen>);
	memcpy( %<outportAddr>, outData, %<outportLen/2>);
    %else
	%if outportType == "int8_T"
	    %%convert int8 to int16
	    for (i = 0; i < %<outportLen>; i++) {
		recbuff[i] = (recbuff[i] & 0x80) ? (recbuff[i] | 0xff00): recbuff[i];
	    }
	%endif
	memcpy( %<outportAddr>, recbuff, %<outportLen>);
    %endif

	%<numport> = numberBytes;

}            
%endfunction %% Outputs

%% [EOF] C28xsci_rx.tlc
