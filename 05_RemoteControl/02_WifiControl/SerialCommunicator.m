classdef SerialCommunicator < handle
    %SERIALCOMMUNICATOR Sends and receives CAN messages predefined in a .dbc
    %file over a serial interface (UDP)
    
    properties (Access = private)
        udps % UDP Object
        BUFFER_udp_input_data
        BUFFER_udp_input_idx
        S2CANbuffer_DATALENGTH
        S2CANbuffer_Bytes
        S2CANbuffer_ID
        S2CANbuffer_INDEX
        S2CANbuffer_Channel
        udpReceiveTimer
        udpReceiveTimerPeriod
    end
    
    methods
        function obj = SerialCommunicator(receive_period)
            obj.BUFFER_udp_input_data = zeros(300, 1, 'uint8');
            obj.BUFFER_udp_input_idx = uint16(0);
            obj.S2CANbuffer_DATALENGTH = zeros(100, 1, 'uint16');
            obj.S2CANbuffer_Bytes = zeros(100, 8, 'uint8');
            obj.S2CANbuffer_ID = zeros(100, 1, 'uint16');
            obj.S2CANbuffer_INDEX = uint16(0);
            obj.S2CANbuffer_Channel
            obj.udpReceiveTimerPeriod = receive_period;
            
        end
        
        function connectToTarget(obj, remoteIPPort, remoteIPAddress)
            obj.udpReceiveTimer = timer('Period', obj.udpReceiveTimerPeriod, 'ExecutionMode', 'FixedRate', 'TimerFcn', @obj.udpReceiveTimerCallback);
            
%             function bytes_available_cb(obj, udp_obj,ev)
%                 disp(num2str(udp_obj.BytesAvailable));
%                 [data, num] = fread(udp_obj, udp_obj.BytesAvailable, 'uint8');
%                 obj.RingBufferWrite_BUFFER_udp_input(data, num);
%             end
            obj.udps = udp(remoteIPAddress, 'RemotePort', remoteIPPort, 'LocalPort', 25000);
%             obj.udps.BytesAvailableFcnMode = 'byte';
%             obj.udps.BytesAvailableFcnCount = 2;
%             obj.udps.BytesAvailableFcn = @(udp_obj, ev)bytes_available_cb(obj,udp_obj,ev);
            obj.udps.ReadAsyncMode = 'continuous';
            obj.udps.InputBufferSize = 1024;
            fopen(obj.udps);
            
            start(obj.udpReceiveTimer);
        end
        
        function disconnect(obj)
            fclose(obj.udps);
            delete(obj.udps);
            stop(obj.udpReceiveTimer);
            delete(obj.udpReceiveTimer);
        end
        
        function conn = isConnected(obj)
            conn = ~isempty(obj.udps);
        end
        
        function hasmsg = hasMessages(obj)
            hasmsg = obj.S2CANbuffer_INDEX~=0;
        end
        
        function sendCANMessage(obj, CanMsg)
            send_string = obj.generateCANString(CanMsg.Data', CanMsg.ID, CanMsg.Length);
%             obj.udps(send_string);
            fwrite(obj.udps, send_string);
        end
        
        function udpReceiveTimerCallback(obj, time, eventData)
            if obj.udps.BytesAvailable > 8
%                 disp(['Bytes Available: ' num2str(obj.udps.BytesAvailable)]);
                [new_data, count] = fread(obj.udps, obj.udps.BytesAvailable, 'uint8');
%                 disp(new_data);
%                 disp(['Bytes read: ' num2str(count)])
%                 disp(['Bytes available: ' num2str(obj.udps.BytesAvailable)])
%                 obj.RingBufferWrite_BUFFER_udp_input(new_data, count);
%                 [data, num] = obj.RingBufferRead_BUFFER_udp_input();
%                 obj.SERIAL2CAN_receive_buffered(data, num, 0);
            end
        end
        
        function y = generateCANString(~, Data, CANID, DATALENGTH)
            %GENERATE_CAN_STRING Summary of this function goes here
            %   Detailed explanation goes here
            %#codegen
            % y = zeros(DATALENGTH + 1+2+1+1,1,'uint8');

            B2 = mod(uint16(CANID),uint16(256));
            B1 = idivide(uint16(CANID),uint16(256));

            y =[uint8(255); uint8([B1; B2]); uint8(DATALENGTH); Data(1:DATALENGTH); uint8(0)];

            y(end) = CRC8(y(2:end-1));
        end
        
        function RingBufferWrite_BUFFER_udp_input(obj, Data_unbuf, num_unbuf) %codegen
            % Write newly arrived data to ring buffer
            for i = 1:num_unbuf
                obj.BUFFER_udp_input_idx = mod(obj.BUFFER_udp_input_idx,length(obj.BUFFER_udp_input_data))+cast(1,'like',obj.BUFFER_udp_input_idx);
                obj.BUFFER_udp_input_data(obj.BUFFER_udp_input_idx) = cast(Data_unbuf(i),'like',obj.BUFFER_udp_input_data);
            end
        end
        
        function [data, num] = RingBufferRead_BUFFER_udp_input(obj) %codegen
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Define Maximum Numer of Elements to be output:
            MaxOutputLength = 100;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            persistent counter_buffer
            if isempty(counter_buffer)
                counter_buffer = cast(0,'like',obj.BUFFER_udp_input_idx);
            end

            data = coder.nullcopy(cast(zeros(MaxOutputLength,1),'like',obj.BUFFER_udp_input_data));
            num = uint32(0);
            for i = 1:length(data)
                if counter_buffer ~= obj.BUFFER_udp_input_idx
                    counter_buffer = mod(counter_buffer,length(obj.BUFFER_udp_input_data))+cast(1,'like',obj.BUFFER_udp_input_idx);
                    data(i) = obj.BUFFER_udp_input_data(counter_buffer);
                    num = num+uint32(1);
                end
            end
        end

        function SERIAL2CAN_receive_buffered(obj, data, num, channel)
            %#codegen
            persistent counter ID_temp CS DATALENGTH Bytes ID
            if isempty(counter)
                CS = uint8(0);
                counter = uint8(0);
                ID_temp = uint8([0 0]);

                DATALENGTH = uint8(0);
                ID = uint16(0);
                Bytes = uint8(zeros(8,1));
            end

            %coder.extrinsic('disp');
            for i = 1:num
            Data = uint8(data(i));
                switch counter
                    case 0
                        if Data == 255
                            counter = uint8(1);
                            CS = uint8(0);
                        end


                    case 1
                        CS = CRC8(Data,CS);
                        ID_temp(1) = Data;
                        counter = uint8(2);
                    case 2
                        CS = CRC8(Data,CS);
                        ID_temp(2) = Data;
                        ID = 256*uint16(ID_temp(1)) + uint16(ID_temp(2));
                        counter = uint8(3);
                    case  3
                        CS = CRC8(Data,CS);
                        DATALENGTH = Data;
                        if DATALENGTH <= 8
                          counter = uint8(4);
                        else
                          counter = uint8(0);
                        end

                    otherwise

                        if counter-3 <= DATALENGTH
                           CS = CRC8(Data,CS);
                           Bytes(counter-3) = Data;
                           counter = counter + uint8(1);
                        else
                           if CS == Data
                               obj.S2CANbuffer_INDEX = mod(obj.S2CANbuffer_INDEX,size(obj.S2CANbuffer_DATALENGTH,1))+uint16(1);    
                               obj.S2CANbuffer_DATALENGTH(obj.S2CANbuffer_INDEX) = DATALENGTH;
                               obj.S2CANbuffer_Bytes(obj.S2CANbuffer_INDEX,:) = Bytes; 
                               obj.S2CANbuffer_ID(obj.S2CANbuffer_INDEX) = ID;
                               obj.S2CANbuffer_Channel(obj.S2CANbuffer_INDEX) = channel;
                           end
                           counter = uint8(0);
                        end

                end
            end
        end
        
%         function fcn1(S2CANbuffer_INDEX, CANID, DATALENGTH, CHANNEL)
%             %#codegen
%             global S2CANbuffer_DATALENGTH S2CANbuffer_Bytes S2CANbuffer_ID  S2CANbuffer_Channel MSGBytes
% 
%             persistent counter
%             if isempty(counter)
%                 counter = uint16(0);
%             end
% 
%             for i = 1:size(S2CANbuffer_DATALENGTH,1)
%                 if counter ~= S2CANbuffer_INDEX
%                     counter = mod(counter,size(S2CANbuffer_DATALENGTH,1))+uint16(1);
% 
%                     if (CANID == S2CANbuffer_ID(counter)) && (DATALENGTH == S2CANbuffer_DATALENGTH(counter) && (CHANNEL == S2CANbuffer_Channel(counter)))
%                         MSGBytes = S2CANbuffer_Bytes(counter,1:length(MSGBytes));
%                         trigger();
%                     end
%                 else
%                     break;
%                 end
%             end
%         end
        
        function [canmsg] = getLastReceivedCANMsg(obj, canmsg, channel)
            maximum_search_iterations = 30;
            search_iterations = 1;
            counter = obj.S2CANbuffer_INDEX;
            
            while (search_iterations < maximum_search_iterations)
                if (canmsg.ID == obj.S2CANbuffer_ID(counter) && canmsg.Length == obj.S2CANbuffer_DATALENGTH(counter) && channel == obj.S2CANbuffer_Channel(counter))
                    canmsg.Data = obj.S2CANbuffer_Bytes(counter,1:obj.S2CANbuffer_DATALENGTH(counter));
                    return;
                end
                
                search_iterations = search_iterations + 1;
            end
        end
    end
end

