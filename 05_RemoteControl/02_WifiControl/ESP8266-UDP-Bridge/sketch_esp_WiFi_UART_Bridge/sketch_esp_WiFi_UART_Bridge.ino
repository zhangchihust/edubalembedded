#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

/////////////// Change these parameters to the respective ID of the MiniSegway /////////////
/////////////// Then just compile and flash the software /////////////////////////////////
#define MINISEGWAY_ID 2
#define MINISEGWAY_SSID "Minisegway02" 

#define MODE_AP // ESP creates wireless access point
//#define MODE_STA // ESP connects to WiFi router

// In both cases the MiniSegway will have the IP 192.168.1.10x (where x is the ID)

// config: ////////////////////////////////////////////////////////////
#define UART_BAUD 921600 
#define packTimeout 5 // ms (if nothing more on UART, then send packet)
#define bufferSize 8192

#define PROTOCOL_UDP

#ifdef MODE_AP
// For AP mode:
const char *ssid = MINISEGWAY_SSID;

//const char *pw = "qwerty123"; // no password required
const char *pw = NULL;
IPAddress ip(192,168,1,100 + MINISEGWAY_ID); 
IPAddress netmask(255, 255, 255, 0);
const int port = 9876; // local port
#endif

#ifdef MODE_STA
// For STATION mode:
const char *ssid = "NETGEAR";  // Your ROUTER SSID
const char *pw = NULL; // and WiFi PASSWORD
const int port = 9876;
IPAddress ip(192,168,1,100 + MINISEGWAY_ID);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);
#endif

//////////////////////////////////////////////////////////////////////////

#ifdef PROTOCOL_UDP
#include <WiFiUdp.h>
WiFiUDP udp;
IPAddress remote_ip;
#endif

// Buffer for received UDP packets
uint8_t buf1[bufferSize];
uint8_t i1=0;
// Buffer for received UART frames
uint8_t buf2[bufferSize];
uint8_t i2=0;

uint8_t SOF1 = 0x3A; //Char ':'; Start of frame bytes
uint8_t SOF2 = 0x7B; //Char '{';
uint8_t EOF1 = 0x7D; //Char '}'; End of frame bytes
uint8_t EOF2 = 0x2D; //Char '-';

bool DATA_READY  = false;

// State machine init
enum state_codes{
  GET_SOF1,
  GET_SOF2,
  GET_DATA,
  GET_EOF2
} STATE;

// Temporal variable to store received UART byte
byte endFrame = 0;
void setup() {

  delay(500); // Wait for startup
  
  Serial.begin(UART_BAUD);

  delay(500); // Make sure to give enough time to the UART module to initialize
  
  #ifdef MODE_AP 
  //AP mode (phone connects directly to ESP) (no router)
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(ip, ip, netmask); // configure ip address for softAP 
  WiFi.softAP(ssid, pw); // configure ssid and password for softAP
  #endif

  
  #ifdef MODE_STA
  // STATION mode (ESP connects to router and gets an IP)
  WiFi.mode(WIFI_STA);
  String connection_msg = "Connecting to ";
  Serial.print(connection_msg + ssid);
  WiFi.config(ip, gateway, subnet, gateway);
  WiFi.begin(ssid, pw);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  Serial.println(WiFi.localIP().toString());
  #endif

  #ifdef PROTOCOL_UDP
  //Serial.println("Starting UDP Server");
  udp.begin(port); // start UDP server 
  #endif
  // Starting state of the state machine
  STATE = GET_SOF1;
}


void loop() {

  #ifdef PROTOCOL_UDP
  // if there’s data available, read a packet
  int packetSize = udp.parsePacket();
  if(packetSize>0) {
    remote_ip = udp.remoteIP(); // store the ip of the remote device
    udp.read(buf1, bufferSize);
    // now send to UART:
    Serial.write(buf1, packetSize);
  }

  // Extract data from the UART Buffer: Data frame should look like this ':{payload}-'
  while(Serial.available()) {
    byte b_data = Serial.read();
    switch(STATE){
      case GET_SOF1:
        if(b_data == SOF1){
          STATE = GET_SOF2;
          i2 = 0;
        }
        else {
          STATE = GET_SOF1;
        }
        break;
      case GET_SOF2:
        if(b_data == SOF2){
          STATE = GET_DATA;
        }
        else {
          STATE = GET_SOF1;
        }
        break;
      case GET_DATA:
        if(b_data != EOF1) {
          buf2[i2] = b_data;
          i2++;
          STATE = GET_DATA;
        }
        else {
          endFrame = b_data;
          STATE = GET_EOF2;
        }
        break;
      case GET_EOF2:
        if(b_data != EOF2){
          buf2[i2] = endFrame;
          buf2[i2 + 1] = b_data;
          i2++;
          STATE = GET_DATA;
        }
        else {
          DATA_READY = true;
        }
        break;
      default:
        break;
    }
  }

  // now send to WiFi: 
  if(DATA_READY) { 
    udp.beginPacket(remote_ip, udp.remotePort()); // remote IP and port
    udp.write(buf2, i2);
    udp.endPacket();
    i2 = 0; // reset the buffer counter
    DATA_READY = false; // set data ready to false to build next data frame
    STATE = GET_SOF2; // reset the state machine to wait for SOF1
  }
  #endif
}
